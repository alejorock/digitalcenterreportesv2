package com.bringsolutions.digitalcenterreportes.cargareportefragments;


import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.adaptadores.ActividadesOrdenesDeServicioAdaptadorSupervisor;
import com.bringsolutions.digitalcenterreportes.adaptadores.OrdenesDeServicioAdaptadorSupervisor;
import com.bringsolutions.digitalcenterreportes.bd.SQLHelper;
import com.bringsolutions.digitalcenterreportes.objetos.ActividadOrdenServicio;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class Actividades extends Fragment {
    View view;

    RecyclerView recyclerView_actividadesOrdenesServicio_Supervisor;
    List<ActividadOrdenServicio> actividadesOrdenServiciosLista = new ArrayList<>();
    ActividadesOrdenesDeServicioAdaptadorSupervisor actividadesOrdenesDeServicioAdaptadorSupervisor;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_actividades, container, false);
        inicializarElementos();
        poblarActividadesDeOrdenServicio();

        return view;
    }

    private void poblarActividadesDeOrdenServicio() {

        AsyncHttpClient client = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL+"mostrar_actividades_xfkorden_servicio.php?fkorden="+Constantes.CLAVE_ORDEN_SELECCIONADA;

        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toasty.warning(getActivity(),"Hubo un problema, intente más tarde.", Toast.LENGTH_LONG, true).show();

            }
        });
    }

    private void obtieneJSON(String response) {
        try{
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length(); i++){
                JSONObject js = jsonArray.getJSONObject(i);

                String nombre= js.getString("os_nombre");
                String actividad = js.getString("act_nombre");
                String paquete = js.getString("paq_nombre");


               // offline(nombre, actividad,paquete);
               actividadesOrdenServiciosLista.add(new ActividadOrdenServicio(nombre, paquete, actividad));
               LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
               recyclerView_actividadesOrdenesServicio_Supervisor.setLayoutManager(layoutManager);

                actividadesOrdenesDeServicioAdaptadorSupervisor = new ActividadesOrdenesDeServicioAdaptadorSupervisor(actividadesOrdenServiciosLista, getActivity());
                recyclerView_actividadesOrdenesServicio_Supervisor.setAdapter(actividadesOrdenesDeServicioAdaptadorSupervisor);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void inicializarElementos() {

        recyclerView_actividadesOrdenesServicio_Supervisor = view.findViewById(R.id.recycler_view_actividades_ordenes_servicio_supervisor);
    }


    private void offline(String nombre, String actividad_nombre, String paq_nombre){
        SQLHelper sqlHelper = new SQLHelper(getContext(), "bd_digital",null,1);

        SQLiteDatabase db = sqlHelper.getWritableDatabase();

        String insert = "INSERT INTO "+Constantes.TABLA_ACTIVIDAD_ORDEN_LOCAL+" ("+Constantes.CAMPO_ACTIVIDAD_ORDEN_NOMBRE+", "+Constantes.CAMPO_ACTIVIDAD_ORDEN_NOMBRE_ACTIVIDAD+", "+Constantes.CAMPO_ACTIVIDAD_ORDEN_NOMBRE_PAQUETE+") "+
                "VALUES ('"+nombre+"', '"+actividad_nombre+"', '"+paq_nombre+"')";

        db.execSQL(insert);
        db.close();
    }
}
