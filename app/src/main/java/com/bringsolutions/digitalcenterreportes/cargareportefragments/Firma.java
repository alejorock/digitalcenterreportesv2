package com.bringsolutions.digitalcenterreportes.cargareportefragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.bd.SQLHelper;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import es.dmoral.toasty.Toasty;

public class Firma extends Fragment {
    View view;
    public static final int SOLICITUD_FIRMA = 150;
    SignaturePad cuadroFirmaRep;
    Button guardarFirmaBoton,limpiarFirmaBoton;

    private FirebaseStorage storage;
    private StorageReference storageReference;
    ProgressDialog progressDialog;

    SQLHelper sqlHelper;

    Constantes constantes = new Constantes();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        sqlHelper = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        view=  inflater.inflate(R.layout.fragment_firma, container, false);

        inicializarElementos();
        clicks();
         return view;
    }

    private void clicks() {
        guardarFirmaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if (constantes.checkInternet(getContext())){
                        subirFotoFirebase(bitmapToUri(getContext(),cuadroFirmaRep.getSignatureBitmap()),SOLICITUD_FIRMA,cuadroFirmaRep.getSignatureBitmap());

                        String u  = String.valueOf(bitmapToUri(getContext(), cuadroFirmaRep.getSignatureBitmap()));
                        offline(u);
                    }else {
                        String u  = String.valueOf(bitmapToUri(getContext(), cuadroFirmaRep.getSignatureBitmap()));
                        offline(u);
                    }

                }catch (Exception e){
                    Toast.makeText(getContext(), "Se produjo un problema a intentar subir, intente nuevamente", Toast.LENGTH_LONG).show();
                    System.out.println("Exeption firma" + e.toString() );
                }

            }
        });
        limpiarFirmaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cuadroFirmaRep.clear();
            }
        });
    }

    private void subirFotoFirebase(Uri uri, final int numFoto, Bitmap foto) {
        storageReference = storage.getReference("reporte_"+ Constantes.OBJETO_REPORTE.getClave());
        switch (numFoto){
            case SOLICITUD_FIRMA:
                procesoSubidaFirebase(uri, SOLICITUD_FIRMA,foto);
                break;
        }
    }

    private byte[] getImageCompressed(Bitmap photo) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, baos);

        return baos.toByteArray();
    }

    private void procesoSubidaFirebase(Uri uri, final int numFoto, Bitmap foto) {
        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());
        final byte[] data = getImageCompressed(foto);
        UploadTask uploadTask = fotoReferencia.putBytes(data);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();

                }

                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                String uriFinal=task.getResult().toString();

                switch (numFoto){
                    case SOLICITUD_FIRMA:
                        Constantes.OBJETO_REPORTE.setFirmavobo(uriFinal);
                        break;
                }

                progressDialog.hide();
                Toast.makeText(getContext(),"Foto cargada!", Toast.LENGTH_LONG).show();

            }
        });
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                double cargado = (taskSnapshot.getBytesTransferred()/1024);
                double total = (taskSnapshot.getTotalByteCount()/1024);
                int cargaActual = (int) cargado;

                progressDialog.setTitle("Subiendo tu foto");
                progressDialog.setMessage(((int)progress) + "% subido...");
                progressDialog.setCancelable(false);
                progressDialog.show();

            }
        });
    }

    private Uri bitmapToUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "firma_vobo", null);
        return Uri.parse(path);
    }

    private void inicializarElementos() {
        cuadroFirmaRep           = view.findViewById(R.id.firmaCuadroReporte);
        guardarFirmaBoton        = view.findViewById(R.id.guardarFirmaBoton);
        limpiarFirmaBoton        = view.findViewById(R.id.limpiarFirmaBoton);
        progressDialog = new ProgressDialog(getActivity());
        storage        = FirebaseStorage.getInstance();

    }
    private void offline(String uri){
        try {
            SQLiteDatabase db = sqlHelper.getWritableDatabase();

            if (!uri.isEmpty()){
                String insert =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                        "SET "+Constantes.REPCR_FIRMA+"= '"+uri+"' " +
                        "WHERE "+Constantes.REPCR_CLAVE+" = '"+Constantes.OBJETO_REPORTE.getClave()+"'";

                db.execSQL(insert);
            }

            db.close();
        }catch (Exception e){
            Toasty.warning(getActivity(), "Ha ocurrido un error.", Toasty.LENGTH_LONG).show();
        }
    }
    private void consultar() {
        try {
            SQLiteDatabase db = sqlHelper.getReadableDatabase();
            String[] parametros = {Constantes.OBJETO_REPORTE.getClave()};

            Cursor cursor = db.rawQuery("SELECT "+Constantes.REPCR_FIRMA+" "+
                    "FROM "+Constantes.TABLA_REPORTE_CREADO+" WHERE "+Constantes.REPCR_CLAVE+" = ?", parametros);

            //if (cursor.getCount()>1){
            cursor.moveToLast();
            String uri1 = cursor.getString(0);


            if (!uri1.isEmpty()){
                Uri u = Uri.parse(uri1);
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                //btnFotoFinalUno.setVisibility(View.GONE);
                //vistaFotoInicialUno.setVisibility(View.VISIBLE);
                //vistaFotoInicialUno.setImageBitmap(bitmap);

            }

        }catch (Exception e){
            Toast.makeText(getContext(), "No hay info", Toast.LENGTH_SHORT).show();
            Log.e("Error excepcion", e.getLocalizedMessage());
        }

    }

    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }
}
