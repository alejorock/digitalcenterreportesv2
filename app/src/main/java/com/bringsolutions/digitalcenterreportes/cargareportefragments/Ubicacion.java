package com.bringsolutions.digitalcenterreportes.cargareportefragments;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.OrdenServicioSupervisor;
import com.bringsolutions.digitalcenterreportes.supervisorsecciones.ordenes.OrdenesSupervisorFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;


public class Ubicacion extends Fragment {
    View view;

    private SupportMapFragment mapFrag;
    private GoogleMap map;
    private Geocoder geocoder;
    private List<Address> addresses;

    Constantes constantes = new Constantes();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ubicacion, container, false);

        if (constantes.checkInternet(getContext())){
            loadMap();
        }else{
            Toasty.warning(getActivity(), "El Mapa requiere conexión a Internet. \nFavor de intentarlo más tarde", Toasty.LENGTH_LONG).show();
            //insertar a la bd los datos del gps provider
            Toasty.error(getActivity(),""+Constantes.USUARIO_INICIO_SESION_LOGIN.getLatitudActual(), Toasty.LENGTH_LONG).show();
        }

        return view;
    }

    private void loadMap() {
        GoogleMapOptions options = new GoogleMapOptions();
        options.zOrderOnTop(true);

        mapFrag =  (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapitaa);

        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                map.clear();
                map.addMarker(new MarkerOptions().position(new LatLng(Constantes.USUARIO_INICIO_SESION_LOGIN.getLatitudActual(), Constantes.USUARIO_INICIO_SESION_LOGIN.getLongitudActual())).title("Mi ubicación actual"));
                CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(new LatLng(Constantes.USUARIO_INICIO_SESION_LOGIN.getLatitudActual(), Constantes.USUARIO_INICIO_SESION_LOGIN.getLongitudActual()), 18);
                map.animateCamera(animacionZoom);

                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        map.clear();
                        map.addMarker(new MarkerOptions().position(latLng).title("Mi nueva ubicación actual").snippet("Nueva ubicación estableida").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(latLng, 18);
                        map.animateCamera(animacionZoom);
                        Constantes.OBJETO_REPORTE.setLatitud(String.valueOf(latLng.latitude));
                        Constantes.OBJETO_REPORTE.setLongitud(String.valueOf(latLng.longitude));

                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        try {
                            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 100);
                            String direccion = addresses.get(0).getAddressLine(0);
                            String codigopostal= addresses.get(0).getPostalCode();
                            if(codigopostal==null){
                                codigopostal="SIN CP";
                            }
                            String   direccionActual = direccion+", "+codigopostal;
                            Constantes.OBJETO_REPORTE.setDireccion(direccionActual);

                        }catch (Exception e){
                            Toasty.info(getContext(), "Ha ocurrido un problema a la hora de obtener la dirección actual, por favor intente nuevamente...", Toast.LENGTH_SHORT, true).show();

                        }

                    }
                });


            }
        });

    }






}
