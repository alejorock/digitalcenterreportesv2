package com.bringsolutions.digitalcenterreportes.cargareportefragments;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bringsolutions.digitalcenterreportes.BuildConfig;
import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.activitysprincipales.Login;
import com.bringsolutions.digitalcenterreportes.bd.SQLHelper;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

import static android.app.Activity.RESULT_OK;

public class EvidenciasIniciales extends Fragment {
    public static final int SOLICITUD_FOTO_INICIAL_1 = 10;
    public static final int SOLICITUD_FOTO_INICIAL_2 = 11;


    View view;

    CardView btnFotoInicialUno,btnFotoInicialDos;
    ImageView vistaFotoInicialUno,vistaFotoInicialDos;

    private Uri uriImagenIncialUno;
    private Uri uriImagenIncialDos;

    int casoTipoFotoSeleccion;

    private StorageReference storageReference;
    private FirebaseStorage storage;


    ProgressDialog progressDialog;
    SQLHelper sqlHelper;
    Constantes constantes = new Constantes();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sqlHelper = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);

        view=  inflater.inflate(R.layout.fragment_evidencias_inciales, container, false);
        inicializarElementos();

        clicks();
        consultar();


        return  view;
    }

    private void clicks() {
        btnFotoInicialUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFoto(1);
            }
        });
        btnFotoInicialDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obtenerFoto(2);
            }
        });

    }

    private void obtenerFoto(final int num_foto) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_tipo_evidencia, null);

        builder.setView(viewDialog);

        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarTipoEvidenciaFotografica);

        ImageView btnArchivos = viewDialog.findViewById(R.id.btnSeleccionarFotoOpcion);
        ImageView btnFotoCamara = viewDialog.findViewById(R.id.btnTomarFotoOpcion);


        final AlertDialog dialog = builder.create();

        btnArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=1;

                verificarPermisosCamaraMemoria(num_foto, 1);
                dialog.dismiss();


            }
        });

        btnFotoCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=2;

                verificarPermisosCamaraMemoria(num_foto, 2);

                dialog.dismiss();

            }
        });


        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }
    private void verificarPermisosCamaraMemoria(int num_foto, int tipoCaso) {
        if (getActivity() != null){
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
            } else {
                abrirCamara(num_foto,tipoCaso);

            }
        }


    }

    private void abrirCamara(int foto, int tipoCaso) {

        if (foto == 1 && tipoCaso == 1) {
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY, uriImagenIncialUno);
            startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), SOLICITUD_FOTO_INICIAL_1);


        } else if (foto == 2 && tipoCaso == 1) {
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY, uriImagenIncialDos);
            startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), SOLICITUD_FOTO_INICIAL_2);

        }else  if(foto==1 && tipoCaso==2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoinicial_uno"+currentDateandTime+".jpg");

            if (getActivity() != null) {
                uriImagenIncialUno = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialUno);
                startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_1);
            }else{
                Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }else  if(foto==2 && tipoCaso==2){

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoinicial_dos"+currentDateandTime+".jpg");
            if (getActivity() != null){
                uriImagenIncialDos = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialDos);
                startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_2);
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //1 = SELECCIÓN DE ARCHIVOS
        //2 = TOMAR FOTO DESDE LA CÁMARA


        if (requestCode == SOLICITUD_FOTO_INICIAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialUno.setVisibility(View.GONE);
                vistaFotoInicialUno.setVisibility(View.VISIBLE);
                vistaFotoInicialUno.setImageBitmap(bitmap);

                if (constantes.checkInternet(getContext())){
                    subirFotoFirebase(u,SOLICITUD_FOTO_INICIAL_1,bitmap);
                    String uri1 = u.toString();
                    offline(uri1,"");
                }else{
                    String uri1 = u.toString();
                    offline(uri1,"");
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if(requestCode == SOLICITUD_FOTO_INICIAL_1 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2){
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialUno);
                btnFotoInicialUno.setVisibility(View.GONE);
                vistaFotoInicialUno.setVisibility(View.VISIBLE);
                vistaFotoInicialUno.setImageBitmap(bitmap);

                if (constantes.checkInternet(getContext())){
                    subirFotoFirebase(uriImagenIncialUno,SOLICITUD_FOTO_INICIAL_1,bitmap);
                    String uri1 = uriImagenIncialUno.toString();
                    offline(uri1,"");
                }else{
                    String uri1 = uriImagenIncialUno.toString();
                    offline(uri1,"");
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if (requestCode == SOLICITUD_FOTO_INICIAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                btnFotoInicialDos.setVisibility(View.GONE);
                vistaFotoInicialDos.setVisibility(View.VISIBLE);
                vistaFotoInicialDos.setImageBitmap(bitmap);
                if (constantes.checkInternet(getContext())){
                    subirFotoFirebase(u,SOLICITUD_FOTO_INICIAL_2,bitmap);
                    String uri2 = u.toString();
                    offline("",uri2);
                }else{
                    String uri2 = u.toString();
                    offline("",uri2);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }else if (requestCode == SOLICITUD_FOTO_INICIAL_2 && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2) {
            try {
                Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialDos);
                btnFotoInicialDos.setVisibility(View.GONE);
                vistaFotoInicialDos.setVisibility(View.VISIBLE);
                vistaFotoInicialDos.setImageBitmap(bitmap);

                if (constantes.checkInternet(getContext())){
                    subirFotoFirebase(uriImagenIncialDos,SOLICITUD_FOTO_INICIAL_2,bitmap);
                    String uri2 = uriImagenIncialDos.toString();
                    offline("",uri2);
                }else{
                    String uri2 = uriImagenIncialDos.toString();
                    offline("",uri2);
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }

    void subirFotoFirebase(Uri uri, final int numFoto, Bitmap foto) {
        storageReference = storage.getReference("reporte_" + Constantes.OBJETO_REPORTE.getClave());//FOTOS SEGÚN SEA EL CASO
        switch (numFoto) {
            case SOLICITUD_FOTO_INICIAL_1:
                procesoSubidaFirebase(uri, SOLICITUD_FOTO_INICIAL_1, foto);
                break;
            case SOLICITUD_FOTO_INICIAL_2:
                procesoSubidaFirebase(uri, SOLICITUD_FOTO_INICIAL_2, foto);
                break;
        }
    }

    private byte[] getImageCompressed(Bitmap photo) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        return baos.toByteArray();
    }

    public void procesoSubidaFirebase(Uri uri, final int numFoto, Bitmap foto) {
        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());
        final byte[] data = getImageCompressed(foto);
        UploadTask uploadTask = fotoReferencia.putBytes(data);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                String uriFinal = task.getResult().toString();

                switch (numFoto) {
                    case SOLICITUD_FOTO_INICIAL_1:
                        Constantes.OBJETO_REPORTE.setFoto_evidencia_inicial_uno(uriFinal);

                        break;
                    case SOLICITUD_FOTO_INICIAL_2:
                        Constantes.OBJETO_REPORTE.setFoto_evidencia_inicial_dos(uriFinal);

                        break;
                }
                progressDialog.hide();
                Toasty.success(getContext(),"Foto cargada!", Toasty.LENGTH_LONG).show();
            }

        });
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                double cargado = (taskSnapshot.getBytesTransferred()/1024);
                double total = (taskSnapshot.getTotalByteCount()/1024);
                int cargaActual = (int) cargado;

                progressDialog.setTitle("Subiendo tu foto");
                progressDialog.setMessage(((int)progress) + "% subido...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                System.out.println("Upload is "+progress+"% done");
            }
        });
    }

    private void inicializarElementos() {
        btnFotoInicialUno = view.findViewById(R.id.btnFotoInicialUno);
        vistaFotoInicialUno = view.findViewById(R.id.vistaImagenIncialUno);
        btnFotoInicialDos= view.findViewById(R.id.btnFotoInicialDos);
        vistaFotoInicialDos = view.findViewById(R.id.vistaImagenIncialDos);

        storage = FirebaseStorage.getInstance();
        progressDialog = new ProgressDialog(getActivity());
    }


    private void offline(String uri, String uri2){
        try {
            SQLiteDatabase db = sqlHelper.getWritableDatabase();

            if (!uri.isEmpty()){
                String insert =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                                "SET "+Constantes.REPCR_FOTO_INICIAL_1+"= '"+uri+"' " +
                                "WHERE "+Constantes.REPCR_CLAVE+" = '"+Constantes.OBJETO_REPORTE.getClave()+"'";

                db.execSQL(insert);
            }else if (!uri2.isEmpty()){
                String insert =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                                "SET "+Constantes.REPCR_FOTO_INICIAL_2+"= '"+uri2+"' " +
                                "WHERE "+Constantes.REPCR_CLAVE+" = '"+Constantes.OBJETO_REPORTE.getClave()+"'";

                db.execSQL(insert);
            }
            db.close();
        }catch (Exception e){
            Toasty.warning(getActivity(), "Ha ocurrido un error.", Toasty.LENGTH_LONG).show();
        }
    }
    private void consultar() {
        try {
            SQLiteDatabase db = sqlHelper.getReadableDatabase();
            String[] parametros = {Constantes.OBJETO_REPORTE.getClave()};

            Cursor cursor = db.rawQuery("SELECT "+Constantes.REPCR_FOTO_INICIAL_1+", "+Constantes.REPCR_FOTO_INICIAL_2+" "+
                    "FROM "+Constantes.TABLA_REPORTE_CREADO+" WHERE "+Constantes.REPCR_CLAVE+" = ?", parametros);

            //if (cursor.getCount()>1){
            cursor.moveToLast();
            String uri1 = cursor.getString(0);
            String uri2 = cursor.getString(1);


            if (!uri1.isEmpty() && !uri2.isEmpty()){
                Uri u2= Uri.parse(uri2);
                Bitmap bitmap1 = decodificarBitmap(getActivity(), u2);
                //btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoInicialDos.setVisibility(View.VISIBLE);
                vistaFotoInicialDos.setImageBitmap(bitmap1);

                Uri u = Uri.parse(uri1);

                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                //btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoInicialUno.setVisibility(View.VISIBLE);
                vistaFotoInicialUno.setImageBitmap(bitmap);
            }else if (!uri1.isEmpty()){
                Uri u = Uri.parse(uri1);
                Bitmap bitmap = decodificarBitmap(getActivity(), u);
                //btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoInicialUno.setVisibility(View.VISIBLE);
                vistaFotoInicialUno.setImageBitmap(bitmap);

            }else if (!uri2.isEmpty()){
                Uri u2= Uri.parse(uri2);
                Bitmap bitmap1 = decodificarBitmap(getActivity(), u2);
                //btnFotoFinalUno.setVisibility(View.GONE);
                vistaFotoInicialDos.setVisibility(View.VISIBLE);
                vistaFotoInicialDos.setImageBitmap(bitmap1);

            }

        }catch (Exception e){
            Toast.makeText(getContext(), "No hay info", Toast.LENGTH_SHORT).show();
            Log.e("Error excepcion", e.getLocalizedMessage());
        }

    }

}
