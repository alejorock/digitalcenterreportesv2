package com.bringsolutions.digitalcenterreportes.supervisorsecciones.ordenes;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.activitysprincipales.Login;
import com.bringsolutions.digitalcenterreportes.adaptadores.OrdenesDeServicioAdaptadorSupervisor;
import com.bringsolutions.digitalcenterreportes.bd.SQLHelper;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.OrdenServicioSupervisor;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class OrdenesSupervisorFragment extends Fragment {
    View view;

    RecyclerView recyclerOrdenesServicio;
    List<OrdenServicioSupervisor> ordenesServiciosLista = new ArrayList<>();
    OrdenesDeServicioAdaptadorSupervisor adaptadorOrdenesServicio;

    RecyclerView recyclerOrdenesServicioOff;
    SQLHelper sqlHelper;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ordenes_supervisor, container, false);
        sqlHelper = new SQLHelper(getActivity(),SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);


        inicializarElementos();
        //poblarOrdenesDeServicio();

        checkConection();
        return view;
    }

    private void checkConection(){
        Constantes constantes = new Constantes();
        if (constantes.checkInternet(getContext())){
            poblarOrdenesDeServicio();

        }else{
            consultarsqliteoffline();

        }

    }
    private void poblarOrdenesDeServicio() {
        AsyncHttpClient client = new AsyncHttpClient();
        String url;

        if (!Constantes.USUARIO_INICIO_SESION_LOGIN.getIdUsuario().equals(null)){
            url = Constantes.URL_PRINCIPAL+"ordenes_x_supervisor.php?fksupervirsor="+Constantes.USUARIO_INICIO_SESION_LOGIN.getIdUsuario();
        }else{
            url = Constantes.URL_PRINCIPAL+"ordenes_x_supervisor.php?fksupervirsor="+ Login.obtenenerPreferenciaID(getContext());

        }


        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toasty.warning(getActivity(),"Hubo un problema, intente más tarde.", Toast.LENGTH_LONG, true).show();

            }
        });

    }

    private void obtieneJSON(String response) {
        try {
                JSONArray jsonArray = new JSONArray(response);
                for (int i=0; i<jsonArray.length();i++)
                {
                    JSONObject js = jsonArray.getJSONObject(i);

                    String os_clave                       = js.getString("os_clave");
                    String os_noOrden            = js.getString("os_noOrden");
                    String os_nombre               = js.getString("os_nombre");
                    String os_fechaIngreso        = js.getString("os_fechaIngreso");
                    String os_fechaProgramada     = js.getString("os_fechaProgramada");
                    String os_fechaFinalizacion   = js.getString("os_fechaFinalizacion");
                    String os_fechaReprogramacion = js.getString("os_fechaReprogramacion");
                    String os_estatus               = js.getString("os_estatus");
                    String os_fkasi           = js.getString("os_fkasi");
                    String os_observaciones        = js.getString("os_observaciones");
                    String os_prioridad            = js.getString("os_prioridad");
                    String asi_fkusu_supervisor            = js.getString("asi_fkusu_supervisor");

                    SQLiteDatabase db = sqlHelper.getWritableDatabase();

                    String insert = "INSERT OR REPLACE INTO "+Constantes.TABLA_ORDENES_SUPERVISOR_LOCAL+" " +
                            "("+Constantes.ORDLCL_CLAVE+", "+Constantes.ORDLCL_ORDEN+", "+Constantes.ORDLCL_NOMBRE +", "+Constantes.ORDLCL_FECHA_INGRESO+", "+Constantes.ORDLCL_FECHA_PROGRAMADA+", "+Constantes.ORDLCL_FECHA_FINALIZACION+","+Constantes.ORDLCL_FECHA_REPROGRAMACION+", "+Constantes.ORDLCL_ESTATUS+", "+Constantes.ORDLCL_FKASI+", "+Constantes.ORDLCL_OBSERVACIONES+", "+Constantes.ORDLCL_PRIORIDAD+", "+Constantes.ORDLCL_ASI_FKUSU_SUPERVISOR+") "+
                            "VALUES ('"+os_clave+"', '"+os_noOrden+"', '"+os_nombre+"', '"+os_fechaIngreso+"', '"+os_fechaProgramada+"', '"+os_fechaFinalizacion+"', '"+os_fechaReprogramacion+"', '"+os_estatus+"', '"+os_fkasi+"', '"+os_observaciones+"', '"+os_prioridad+"', '"+asi_fkusu_supervisor+"')";


                    db.execSQL(insert);
                    db.close();

                    ordenesServiciosLista.add(new OrdenServicioSupervisor(os_clave,os_noOrden, os_nombre, os_fechaIngreso, os_fechaProgramada, os_fechaFinalizacion, os_fechaReprogramacion,os_estatus,os_fkasi,os_observaciones,os_prioridad,asi_fkusu_supervisor));

                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                    layoutManager.setReverseLayout(true);
                    recyclerOrdenesServicio.setHasFixedSize(true);
                    recyclerOrdenesServicio.setLayoutManager(layoutManager);
                    adaptadorOrdenesServicio = new OrdenesDeServicioAdaptadorSupervisor(ordenesServiciosLista, getContext());
                    recyclerOrdenesServicio.setAdapter(adaptadorOrdenesServicio);
                }
            }catch (Exception e){
                Toasty.warning(getContext(),"Hubo un problema, intente más tarde.", Toast.LENGTH_LONG, true).show();
        }
    }


    private void inicializarElementos() {
        recyclerOrdenesServicio = view.findViewById(R.id.recycler_ordenes_servicio_supervisor);
        recyclerOrdenesServicioOff = view.findViewById(R.id.recycler_ordenes_servicio_supervisor);

    }

    private void offline(String uri, String uri2){

        try {
            SQLHelper sqlHelper = new SQLHelper(getContext(), "bd_digital", null,1);
            SQLiteDatabase db = sqlHelper.getWritableDatabase();

            //String insert = "INSERT INTO "+Constantes.TABLA_REPORTE_CREADO+" ("+Constantes.REPCR_FOTO_FINAL_1+", "+Constantes.REPCR_FOTO_FINAL_2+") "+
            //        "VALUES ('"+uri+"', '"+uri2+"') WHERE "+Constantes.REPCR_CLAVE+" = "+Constantes.OBJETO_REPORTE.getClave()+"";

            String insert =
                    "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                            "SET "+Constantes.REPCR_FOTO_FINAL_1+"= '"+uri+"', "+Constantes.REPCR_FOTO_FINAL_2+"= '"+uri2+"' " +
                            "WHERE "+Constantes.REPCR_CLAVE+" = '"+Constantes.OBJETO_REPORTE.getClave()+"'";

            db.execSQL(insert);
            db.close();
        }catch (Exception e){
            Log.e("Error insert", e.getLocalizedMessage());
        }
    }

    private void consultarsqliteoffline() {
        SQLiteDatabase db = sqlHelper.getReadableDatabase();
        String[] parametros = {Constantes.OBJETO_REPORTE.getUsuario()};

        Cursor cursor = db.rawQuery("SELECT * FROM "+Constantes.TABLA_ORDENES_SUPERVISOR_LOCAL, null);

        try {
            while (cursor.moveToNext()){
                OrdenServicioSupervisor ordenServicioSupervisor = new OrdenServicioSupervisor();
                ordenServicioSupervisor.setOs_clave(cursor.getString(0).trim());
                ordenServicioSupervisor.setOs_noOrden(cursor.getString(1).trim());
                ordenServicioSupervisor.setOs_nombre(cursor.getString(2).trim());
                ordenServicioSupervisor.setOs_fechaIngreso(cursor.getString(3).trim());
                ordenServicioSupervisor.setOs_fechaProgramada(cursor.getString(4).trim());
                ordenServicioSupervisor.setOs_fechaFinalizacion(cursor.getString(5).trim());
                ordenServicioSupervisor.setOs_fechaReprogramacion(cursor.getString(6).trim());
                ordenServicioSupervisor.setOs_estatus(cursor.getString(7).trim());
                ordenServicioSupervisor.setOs_fkasi(cursor.getString(8).trim());
                ordenServicioSupervisor.setOs_observaciones(cursor.getString(9).trim());
                ordenServicioSupervisor.setOs_prioridad(cursor.getString(10).trim());
                ordenServicioSupervisor.setAsi_fkusu_supervisor(cursor.getString(11).trim());

                ordenesServiciosLista.add(ordenServicioSupervisor);

                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                layoutManager.setReverseLayout(true);
                recyclerOrdenesServicio.setHasFixedSize(true);
                recyclerOrdenesServicio.setLayoutManager(layoutManager);
                adaptadorOrdenesServicio = new OrdenesDeServicioAdaptadorSupervisor(ordenesServiciosLista, getActivity());
                recyclerOrdenesServicio.setAdapter(adaptadorOrdenesServicio);
                Toasty.info(getActivity(),"Estás en el modo sin conexión.",Toasty.LENGTH_LONG).show();

            }
            //cargaListaOffline();

        }catch (Exception e) {
            Toasty.warning(getContext(),"No hay datos sincronizados \n Intente más tarde.", Toast.LENGTH_LONG, true).show();
            db.close();
        }
    }
}