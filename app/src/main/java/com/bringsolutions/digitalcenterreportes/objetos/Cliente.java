package com.bringsolutions.digitalcenterreportes.objetos;

public class Cliente {

    private String os_clave;
    private String os_noOrden;
    private String os_nombre;
    private String os_fechaIngreso;
    private String os_fechaProgramada;
    private String os_fechaFinalizacion;
    private String os_fechaReprogramacion;
    private String os_estatus;
    private String os_fkasi;
    private String os_observaciones;
    private String os_prioridad;
    private String asi_clave;
    private String asi_fkest;
    private String asi_fkusu_supervisor;
    private String asi_fkveh;
    private String est_clave;
    private String est_region;
    private String est_noCliente;
    private String est_nomCliente;
    private String est_nomCliente2;
    private String est_calle;
    private String est_entreCalle;
    private String est_yCalle;
    private String est_noExterno;
    private String est_colonia;
    private String est_poblacion;
    private String est_cp;
    private String est_descripcion;
    private String est_latitud;
    private String est_longitud;
    private String est_telefono;
    private String est_fkger;

    public Cliente(){}

    public Cliente(String os_clave, String os_noOrden, String os_nombre, String os_fechaIngreso, String os_fechaProgramada, String os_fechaFinalizacion, String os_fechaReprogramacion, String os_estatus, String os_fkasi, String os_observaciones, String os_prioridad, String asi_clave, String asi_fkest, String asi_fkusu_supervisor, String asi_fkveh, String est_clave, String est_region, String est_noCliente, String est_nomCliente, String est_nomCliente2, String est_calle, String est_entreCalle, String est_yCalle, String est_noExterno, String est_colonia, String est_poblacion, String est_cp, String est_descripcion, String est_latitud, String est_longitud, String est_telefono, String est_fkger) {
        this.os_clave = os_clave;
        this.os_noOrden = os_noOrden;
        this.os_nombre = os_nombre;
        this.os_fechaIngreso = os_fechaIngreso;
        this.os_fechaProgramada = os_fechaProgramada;
        this.os_fechaFinalizacion = os_fechaFinalizacion;
        this.os_fechaReprogramacion = os_fechaReprogramacion;
        this.os_estatus = os_estatus;
        this.os_fkasi = os_fkasi;
        this.os_observaciones = os_observaciones;
        this.os_prioridad = os_prioridad;
        this.asi_clave = asi_clave;
        this.asi_fkest = asi_fkest;
        this.asi_fkusu_supervisor = asi_fkusu_supervisor;
        this.asi_fkveh = asi_fkveh;
        this.est_clave = est_clave;
        this.est_region = est_region;
        this.est_noCliente = est_noCliente;
        this.est_nomCliente = est_nomCliente;
        this.est_nomCliente2 = est_nomCliente2;
        this.est_calle = est_calle;
        this.est_entreCalle = est_entreCalle;
        this.est_yCalle = est_yCalle;
        this.est_noExterno = est_noExterno;
        this.est_colonia = est_colonia;
        this.est_poblacion = est_poblacion;
        this.est_cp = est_cp;
        this.est_descripcion = est_descripcion;
        this.est_latitud = est_latitud;
        this.est_longitud = est_longitud;
        this.est_telefono = est_telefono;
        this.est_fkger = est_fkger;
    }

    public String getOs_clave() {
        return os_clave;
    }

    public void setOs_clave(String os_clave) {
        this.os_clave = os_clave;
    }

    public String getOs_noOrden() {
        return os_noOrden;
    }

    public void setOs_noOrden(String os_noOrden) {
        this.os_noOrden = os_noOrden;
    }

    public String getOs_nombre() {
        return os_nombre;
    }

    public void setOs_nombre(String os_nombre) {
        this.os_nombre = os_nombre;
    }

    public String getOs_fechaIngreso() {
        return os_fechaIngreso;
    }

    public void setOs_fechaIngreso(String os_fechaIngreso) {
        this.os_fechaIngreso = os_fechaIngreso;
    }

    public String getOs_fechaProgramada() {
        return os_fechaProgramada;
    }

    public void setOs_fechaProgramada(String os_fechaProgramada) {
        this.os_fechaProgramada = os_fechaProgramada;
    }

    public String getOs_fechaFinalizacion() {
        return os_fechaFinalizacion;
    }

    public void setOs_fechaFinalizacion(String os_fechaFinalizacion) {
        this.os_fechaFinalizacion = os_fechaFinalizacion;
    }

    public String getOs_fechaReprogramacion() {
        return os_fechaReprogramacion;
    }

    public void setOs_fechaReprogramacion(String os_fechaReprogramacion) {
        this.os_fechaReprogramacion = os_fechaReprogramacion;
    }

    public String getOs_estatus() {
        return os_estatus;
    }

    public void setOs_estatus(String os_estatus) {
        this.os_estatus = os_estatus;
    }

    public String getOs_fkasi() {
        return os_fkasi;
    }

    public void setOs_fkasi(String os_fkasi) {
        this.os_fkasi = os_fkasi;
    }

    public String getOs_observaciones() {
        return os_observaciones;
    }

    public void setOs_observaciones(String os_observaciones) {
        this.os_observaciones = os_observaciones;
    }

    public String getOs_prioridad() {
        return os_prioridad;
    }

    public void setOs_prioridad(String os_prioridad) {
        this.os_prioridad = os_prioridad;
    }

    public String getAsi_clave() {
        return asi_clave;
    }

    public void setAsi_clave(String asi_clave) {
        this.asi_clave = asi_clave;
    }

    public String getAsi_fkest() {
        return asi_fkest;
    }

    public void setAsi_fkest(String asi_fkest) {
        this.asi_fkest = asi_fkest;
    }

    public String getAsi_fkusu_supervisor() {
        return asi_fkusu_supervisor;
    }

    public void setAsi_fkusu_supervisor(String asi_fkusu_supervisor) {
        this.asi_fkusu_supervisor = asi_fkusu_supervisor;
    }

    public String getAsi_fkveh() {
        return asi_fkveh;
    }

    public void setAsi_fkveh(String asi_fkveh) {
        this.asi_fkveh = asi_fkveh;
    }

    public String getEst_clave() {
        return est_clave;
    }

    public void setEst_clave(String est_clave) {
        this.est_clave = est_clave;
    }

    public String getEst_region() {
        return est_region;
    }

    public void setEst_region(String est_region) {
        this.est_region = est_region;
    }

    public String getEst_noCliente() {
        return est_noCliente;
    }

    public void setEst_noCliente(String est_noCliente) {
        this.est_noCliente = est_noCliente;
    }

    public String getEst_nomCliente() {
        return est_nomCliente;
    }

    public void setEst_nomCliente(String est_nomCliente) {
        this.est_nomCliente = est_nomCliente;
    }

    public String getEst_nomCliente2() {
        return est_nomCliente2;
    }

    public void setEst_nomCliente2(String est_nomCliente2) {
        this.est_nomCliente2 = est_nomCliente2;
    }

    public String getEst_calle() {
        return est_calle;
    }

    public void setEst_calle(String est_calle) {
        this.est_calle = est_calle;
    }

    public String getEst_entreCalle() {
        return est_entreCalle;
    }

    public void setEst_entreCalle(String est_entreCalle) {
        if(est_entreCalle.equals("0") || est_entreCalle.length()<3 || est_entreCalle.equals("") || est_entreCalle.equals("null") || est_entreCalle==null){
            est_entreCalle="Sin establecer";
        }

        this.est_entreCalle = est_entreCalle;
    }

    public String getEst_yCalle() {
        return est_yCalle;
    }

    public void setEst_yCalle(String est_yCalle) {
        if(est_yCalle.equals("0") || est_yCalle.length()<3 || est_yCalle.equals("") || est_yCalle.equals("null") || est_yCalle==null){
            est_yCalle="Sin establecer";
        }
        this.est_yCalle = est_yCalle;
    }

    public String getEst_noExterno() {
        return est_noExterno;
    }

    public void setEst_noExterno(String est_noExterno) {
        this.est_noExterno = est_noExterno;
    }

    public String getEst_colonia() {
        return est_colonia;
    }

    public void setEst_colonia(String est_colonia) {
        if(est_colonia.equals("0") || est_colonia.length()<3 || est_colonia.equals("") || est_colonia.equals("null") || est_colonia==null){
            est_colonia="Sin establecer";
        }
        this.est_colonia = est_colonia;
    }

    public String getEst_poblacion() {
        return est_poblacion;
    }

    public void setEst_poblacion(String est_poblacion) {
        this.est_poblacion = est_poblacion;
    }

    public String getEst_cp() {
        return est_cp;
    }

    public void setEst_cp(String est_cp) {
        if(est_cp.equals("0") || est_cp.length()<3 || est_cp.equals("") || est_cp.equals("null") || est_cp==null){
            est_cp="Sin establecer";
        }
        this.est_cp = est_cp;
    }

    public String getEst_descripcion() {
        return est_descripcion;
    }

    public void setEst_descripcion(String est_descripcion) {
        this.est_descripcion = est_descripcion;
    }

    public String getEst_latitud() {
        return est_latitud;
    }

    public void setEst_latitud(String est_latitud) {
        this.est_latitud = est_latitud;
    }

    public String getEst_longitud() {
        return est_longitud;
    }

    public void setEst_longitud(String est_longitud) {
        this.est_longitud = est_longitud;
    }

    public String getEst_telefono() {
        return est_telefono;
    }

    public void setEst_telefono(String est_telefono) {
        this.est_telefono = est_telefono;
    }

    public String getEst_fkger() {
        return est_fkger;
    }

    public void setEst_fkger(String est_fkger) {
        this.est_fkger = est_fkger;
    }
}
