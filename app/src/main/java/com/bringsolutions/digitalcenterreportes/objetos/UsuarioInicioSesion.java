package com.bringsolutions.digitalcenterreportes.objetos;

public class UsuarioInicioSesion {
    String nombreUsuario;
    String tipoUsuario;
    String direccionActual;
    String idUsuario;
    Double latitudActual, longitudActual;

    public UsuarioInicioSesion(){

    }


    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDireccionActual() {
        return direccionActual;
    }

    public void setDireccionActual(String direccionActual) {
        this.direccionActual = direccionActual;
    }

    public Double getLatitudActual() {
        return latitudActual;
    }

    public void setLatitudActual(Double latitudActual) {
        this.latitudActual = latitudActual;
    }

    public Double getLongitudActual() {
        return longitudActual;
    }

    public void setLongitudActual(Double longitudActual) {
        this.longitudActual = longitudActual;
    }
}
