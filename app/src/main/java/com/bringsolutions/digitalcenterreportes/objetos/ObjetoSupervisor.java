package com.bringsolutions.digitalcenterreportes.objetos;

public class ObjetoSupervisor {

    private String idSupervisor;
    private String nombreSupervisor;
    private String usuarioNombreSupervisor;
    private String statusSupervisor;
    private String emailSupervisor;
    private String fkGerSupervisor;

    public ObjetoSupervisor(){}

    public ObjetoSupervisor(String idSupervisor, String nombreSupervisor, String usuarioNombreSupervisor, String statusSupervisor, String emailSupervisor, String fkGerSupervisor) {
        this.idSupervisor = idSupervisor;
        this.nombreSupervisor = nombreSupervisor;
        this.usuarioNombreSupervisor = usuarioNombreSupervisor;
        this.statusSupervisor = statusSupervisor;
        this.emailSupervisor = emailSupervisor;
        this.fkGerSupervisor = fkGerSupervisor;
    }

    public String getIdSupervisor() {
        return idSupervisor;
    }

    public void setIdSupervisor(String idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    public String getNombreSupervisor() {
        return nombreSupervisor;
    }

    public void setNombreSupervisor(String nombreSupervisor) {
        this.nombreSupervisor = nombreSupervisor;
    }

    public String getUsuarioNombreSupervisor() {
        return usuarioNombreSupervisor;
    }

    public void setUsuarioNombreSupervisor(String usuarioNombreSupervisor) {
        this.usuarioNombreSupervisor = usuarioNombreSupervisor;
    }

    public String getStatusSupervisor() {
        return statusSupervisor;
    }

    public void setStatusSupervisor(String statusSupervisor) {
        this.statusSupervisor = statusSupervisor;
    }

    public String getEmailSupervisor() {
        return emailSupervisor;
    }

    public void setEmailSupervisor(String emailSupervisor) {
        this.emailSupervisor = emailSupervisor;
    }

    public String getFkGerSupervisor() {
        return fkGerSupervisor;
    }

    public void setFkGerSupervisor(String fkGerSupervisor) {
        this.fkGerSupervisor = fkGerSupervisor;
    }
}
