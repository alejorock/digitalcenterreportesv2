package com.bringsolutions.digitalcenterreportes.objetos;

public class Notificacion {

    String remitente_id;
    String titulo_mensaje;
    String cuerpo_mensaje;
    String fecha;
    String destinatario_id;
    String propietarioNoti;

    public Notificacion(){

    }

    public Notificacion(String remitente_id, String titulo_mensaje, String cuerpo_mensaje, String fecha, String destinatario_id, String propietarioNoti) {
        this.remitente_id = remitente_id;
        this.titulo_mensaje = titulo_mensaje;
        this.cuerpo_mensaje = cuerpo_mensaje;
        this.fecha = fecha;
        this.destinatario_id = destinatario_id;
        this.propietarioNoti = propietarioNoti;
    }

    public String getRemitente_id() {
        return remitente_id;
    }

    public void setRemitente_id(String remitente_id) {
        this.remitente_id = remitente_id;
    }

    public String getTitulo_mensaje() {
        return titulo_mensaje;
    }

    public void setTitulo_mensaje(String titulo_mensaje) {
        this.titulo_mensaje = titulo_mensaje;
    }

    public String getCuerpo_mensaje() {
        return cuerpo_mensaje;
    }

    public void setCuerpo_mensaje(String cuerpo_mensaje) {
        this.cuerpo_mensaje = cuerpo_mensaje;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDestinatario_id() {
        return destinatario_id;
    }

    public void setDestinatario_id(String destinatario_id) {
        this.destinatario_id = destinatario_id;
    }

    public String getPropietarioNoti() {
        return propietarioNoti;
    }

    public void setPropietarioNoti(String propietarioNoti) {
        this.propietarioNoti = propietarioNoti;
    }
}
