package com.bringsolutions.digitalcenterreportes.objetos;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

public class Constantes {

    public final static String  URL_PRINCIPAL ="http://vigux.com.mx/dc/ws/";
    public final static UsuarioInicioSesion USUARIO_INICIO_SESION_LOGIN = new UsuarioInicioSesion();
    public static String CLAVE_ORDEN_SELECCIONADA = "";
    public final static Reporte OBJETO_REPORTE =new Reporte();
    public final static OrdenServicioSupervisor ORDEN_SERVICIO_SUPERVISOR = new OrdenServicioSupervisor();
    public static String CLAVE_REPORTE_VACIO_CREADO="";
    public static boolean STATUS_ENVIO_UBICACION=false;



    /*/* --------------------*/

    public static final String TABLA_REPORTE_CREADO = "reporte_creado";

    public static final String REPCR_CLAVE= "clave";
    public static final String REPCR_USUARIO = "usuario";
    public static final String REPCR_FOLIO   = "folio";
    public static final String REPCR_DIRECCION = "direccion";
    public static final String REPCR_LATITUD   = "latitud";
    public static final String REPCR_LONGITUD  = "longitud";
    public static final String REPCR_FOTO_INICIAL_1 = "foto_evidencia_inicial_uno";
    public static final String REPCR_FOTO_INICIAL_2 = "foto_evidencia_inicial_dos";
    public static final String REPCR_FOTO_FINAL_1 = "foto_evidencia_final_uno";
    public static final String REPCR_FOTO_FINAL_2 = "foto_evidencia_final_dos";
    public static final String REPCR_FIRMA          = "firmavobo";
    public static final String REPCR_FOTO_INE_1 =  "foto_ine_anverso";
    public static final String REPCR_FOTO_INE_2 = "foto_ine_reverso";
    public static final String REPCR_ORDEN  = "orden";
    public static final String RPPCR_FECHA_BD_LOCAL = "fecha_local";
    public static final String RPPCR_ESTADO_ENVIO = "estado_envio";

    public static final String CREAR_TABLA_REPORTE_CREADO = "CREATE TABLE "+TABLA_REPORTE_CREADO+" ("+REPCR_CLAVE+" TEXT UNIQUE, " +
            ""+REPCR_USUARIO+" TEXT, "+REPCR_FOLIO+" TEXT, " +
            ""+REPCR_DIRECCION+" TEXT, "+REPCR_LATITUD+" TEXT, "+REPCR_LONGITUD+" TEXT, "+REPCR_FOTO_INICIAL_1+" TEXT, " +
            ""+REPCR_FOTO_INICIAL_2+" TEXT, "+REPCR_FOTO_FINAL_1+" TEXT, "+REPCR_FOTO_FINAL_2+" TEXT, " +
            ""+REPCR_FIRMA+" TEXT, "+REPCR_FOTO_INE_1+" TEXT, "+REPCR_FOTO_INE_2+" TEXT, "+REPCR_ORDEN+" TEXT, "+RPPCR_FECHA_BD_LOCAL+" TEXT, "+RPPCR_ESTADO_ENVIO+" TEXT );";

    /*/* --------------------*/


    public static final String TABLA_ORDENES_SUPERVISOR_LOCAL = "ordenes_supervisor_local";

    public static final String ORDLCL_CLAVE ="os_clave";
    public static final String ORDLCL_ORDEN = "os_noOrden";
    public static final String ORDLCL_NOMBRE= "os_nombre";
    public static final String ORDLCL_FECHA_INGRESO = "os_fechaIngreso";
    public static final String ORDLCL_FECHA_PROGRAMADA = "os_fechaProgramada";
    public static final String ORDLCL_FECHA_FINALIZACION = "os_fechaFinalizacion";
    public static final String ORDLCL_FECHA_REPROGRAMACION = "os_fechaReprogramacion";
    public static final String ORDLCL_ESTATUS = "os_status";
    public static final String ORDLCL_FKASI = "os_fkasi";
    public static final String ORDLCL_OBSERVACIONES = "os_observaciones";
    public static final String ORDLCL_PRIORIDAD = "os_prioridad";
    public static final String ORDLCL_ASI_FKUSU_SUPERVISOR ="asi_fkusu_supervisor";

    public static final String CREAR_TABLA_ORDENES_LOCAL = "CREATE TABLE "+TABLA_ORDENES_SUPERVISOR_LOCAL+" ("+ORDLCL_CLAVE+" TEXT UNIQUE, "+ORDLCL_ORDEN+" TEXT, "+ORDLCL_NOMBRE+" TEXT, "+ORDLCL_FECHA_INGRESO+" TEXT, "+ORDLCL_FECHA_PROGRAMADA+ " TEXT, " +ORDLCL_FECHA_FINALIZACION+" TEXT, "+ORDLCL_FECHA_REPROGRAMACION+" TEXT, "+ORDLCL_ESTATUS+" TEXT, "+ORDLCL_FKASI+ " TEXT, "+ORDLCL_OBSERVACIONES+ " TEXT, " +ORDLCL_PRIORIDAD+" TEXT, "+ORDLCL_ASI_FKUSU_SUPERVISOR+" TEXT)";


    /*/* --------------------*/

    public static final String TABLA_ORDENES_DIGITAL_LOCAL = "ordenes_digital_local";

    public static final String CAMPO_ORDENDIGITAL_CLAVE = "os_clave";
    public static final String CAMPO_ORDENDIGITAL_NOORDEN = "os_noOrden";
    public static final String CAMPO_ORDENDIGITAL_NOMBRE = "os_nombre";
    public static final String CAMPO_ORDENDIGITAL_FECHAINGRESO = "os_fechaIngreso";
    public static final String CAMPO_ORDENDIGITAL_FECHAPROGRAMADA = "os_fechaProgramada";
    public static final String CAMPO_ORDENDIGITAL_FECHAFINALIZACION = "os_fechaFinalizacion";
    public static final String CAMPO_ORDENDIGITAL_FECHAREPROGRAMACION = "os_fechaReprogramacion";
    public static final String CAMPO_ORDENDIGITAL_ESTATUS = "os_estatus";
    public static final String CAMPO_ORDENDIGITAL_FKASIG = "os_fk_asi";
    public static final String CAMPO_ORDENDIGITAL_OBSERVACIONES = "os_observaciones";
    public static final String CAMPO_ORDENDIGITAL_PRIORIDAD = "os_prioridad";


    public static final String CREAR_TABLA_ORDENES_DIGITAL_LOCAL = "CREATE TABLE "+TABLA_ORDENES_DIGITAL_LOCAL+" (idorden INTEGER PRIMARY KEY AUTOINCREMENT, "+
            ""+CAMPO_ORDENDIGITAL_CLAVE+" TEXT, "+CAMPO_ORDENDIGITAL_NOORDEN+" TEXT, "+CAMPO_ORDENDIGITAL_NOMBRE+" TEXT, "+CAMPO_ORDENDIGITAL_FECHAINGRESO+" TEXT, "+
            ""+CAMPO_ORDENDIGITAL_FECHAPROGRAMADA+" TEXT, "+CAMPO_ORDENDIGITAL_FECHAFINALIZACION+" TEXT, "+CAMPO_ORDENDIGITAL_FECHAREPROGRAMACION+" TEXT, "+
            ""+CAMPO_ORDENDIGITAL_ESTATUS+" TEXT, "+CAMPO_ORDENDIGITAL_FKASIG+" TEXT, "+CAMPO_ORDENDIGITAL_OBSERVACIONES+" TEXT, "+CAMPO_ORDENDIGITAL_PRIORIDAD+" TEXT);";



    public static final String TABLA_ACTIVIDAD_ORDEN_LOCAL = "actividad_ordenes_local";

    public static final String CAMPO_ACTIVIDAD_ORDEN_NOMBRE = "os_nombre";
    public static final String CAMPO_ACTIVIDAD_ORDEN_NOMBRE_PAQUETE = "paq_nombre";
    public static final String CAMPO_ACTIVIDAD_ORDEN_NOMBRE_ACTIVIDAD = "act_nombre";

    public static final String CREAR_TABLA_ACTIVIDADES_ORDEN = "CREATE TABLE "+TABLA_ACTIVIDAD_ORDEN_LOCAL+" (idactividad INTEGER PRIMARY KEY AUTOINCREMENT, "+
            ""+CAMPO_ACTIVIDAD_ORDEN_NOMBRE+" TEXT, "+CAMPO_ACTIVIDAD_ORDEN_NOMBRE_PAQUETE+" TEXT, "+CAMPO_ACTIVIDAD_ORDEN_NOMBRE_ACTIVIDAD+" TEXT);";


    /*/* --------------------*/
    public static final String TABLA_USUARIOS_LOCAL = "usuarios_local";

    public static final String CAMPO_USUARIO_ID = "id";
    public static final String CAMPO_USUARIO_NOMBRE = "nombre";
    public static final String CAMPO_USUARIO_TIPO = "tipo";

    public static final String CREAR_TABLA_USUARIOS_LOCAL = "CREATE TABLE "+TABLA_USUARIOS_LOCAL+" ("+CAMPO_USUARIO_ID+" TEXT, "+CAMPO_USUARIO_NOMBRE+" TEXT, "+CAMPO_USUARIO_TIPO+" TEXT)";


    /****/

    public boolean checkInternet(Context context) {
        boolean conn = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            conn = true;
        } else conn = false;

        return conn;
    }


}
