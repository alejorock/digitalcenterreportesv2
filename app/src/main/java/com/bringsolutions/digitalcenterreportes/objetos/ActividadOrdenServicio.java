package com.bringsolutions.digitalcenterreportes.objetos;

public class ActividadOrdenServicio {
    String os_nombre, paq_nombre, act_nombre;

    public ActividadOrdenServicio(){}

    public ActividadOrdenServicio(String os_nombre, String paq_nombre, String act_nombre) {
        this.os_nombre = os_nombre;
        this.paq_nombre = paq_nombre;
        this.act_nombre = act_nombre;
    }

    public String getOs_nombre() {
        return os_nombre;
    }

    public void setOs_nombre(String os_nombre) {
        this.os_nombre = os_nombre;
    }

    public String getPaq_nombre() {
        return paq_nombre;
    }

    public void setPaq_nombre(String paq_nombre) {
        this.paq_nombre = paq_nombre;
    }

    public String getAct_nombre() {
        return act_nombre;
    }

    public void setAct_nombre(String act_nombre) {
        this.act_nombre = act_nombre;
    }
}