package com.bringsolutions.digitalcenterreportes.activitysreportes;
//COMMIT EN CASO DE QUE LLEGUE A FALLAR ALGO, RESTABLECER EN ESTE COMMIT <3

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.animation.content.Content;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.digitalcenterreportes.BuildConfig;
import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.activitysprincipales.Login;
import com.bringsolutions.digitalcenterreportes.activitysprincipales.Supervisor;
import com.bringsolutions.digitalcenterreportes.bd.SQLHelper;
import com.bringsolutions.digitalcenterreportes.cargareportefragments.Actividades;
import com.bringsolutions.digitalcenterreportes.cargareportefragments.EvidenciasFinales;
import com.bringsolutions.digitalcenterreportes.cargareportefragments.EvidenciasIniciales;
import com.bringsolutions.digitalcenterreportes.cargareportefragments.Firma;
import com.bringsolutions.digitalcenterreportes.cargareportefragments.INE;
import com.bringsolutions.digitalcenterreportes.cargareportefragments.Ubicacion;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.Reporte;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.joaquimley.faboptions.FabOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class RealizarReporte extends AppCompatActivity implements View.OnClickListener {

    ArrayList<String> listaIncidencias = new ArrayList<>();
     AlertDialog dialogIncidencias;

    private Uri uriImagenActividadExtra;
    static final int SOLICITUD_FOTO_ACT_EXTRA = 1;

    ImageButton btnImagenActividadExtra;
    ImageView vistaImagenActividadExtra;
    EditText cajaDescripcionActividad;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    CardView btnMiUbicacion, btnEvidenciasIniciales, btnActividades, btnFirma, btnINEFotos, btnEvidenciasFinales, btnSubirRepote;
    TextView tvTextoUbicacion, tvFotosInciales, tvActividades, tvFirma, tvINEFotos, tvFotosFinales;
    ProgressBar progressBarReporteSupervisor;
    ScrollView scrollReporte;
    FabOptions botonFlotante;

    int progresoReporte=0;


    int casoTipoFotoSeleccion;

    private StorageReference storageReference;
    private FirebaseStorage storage;


    ProgressDialog progressDialog;
    String descripcionActiExtra;
    String uriFinalActExtra;

    SQLHelper sqlHelper;
    AlertDialog dialogReporte;



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realizar_reporte);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sqlHelper = new SQLHelper(this,SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
        inicializarElementos();


        Intent intentRecibir= getIntent();
        String claveOrdenRecibida = intentRecibir.getStringExtra("ID_CLAVE_ORDEN");

        crearReporteVacio(claveOrdenRecibida);//CREO UN REPORTE VACÍO CON RESPECTO AL ID DE LA ORDEN DE SERVICIO

        Constantes.CLAVE_ORDEN_SELECCIONADA= claveOrdenRecibida;
        getSupportActionBar().setTitle("Realizar Reporte : " + claveOrdenRecibida);
        clicks();

        verificarInternet();

    }

    private void verificarInternet(){
        Constantes constantes = new Constantes();
        if (constantes.checkInternet(getApplicationContext())){
            consultarPendientes();

        }else{
            sincronizarSQL();

        }
    }

    private void clicks() {
        btnMiUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(tvTextoUbicacion.getText().equals("Ubicación")){
                    findViewById(R.id.separador_imagen).setVisibility(View.GONE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.VISIBLE);
                    tvTextoUbicacion.setText("Volver al menú");
                    cargarFragment(new Ubicacion());
                    btnEvidenciasIniciales.setVisibility(View.GONE);
                    btnActividades.setVisibility(View.GONE);
                    btnFirma.setVisibility(View.GONE);
                    btnEvidenciasFinales.setVisibility(View.GONE);
                    btnINEFotos.setVisibility(View.GONE);

                }else if(tvTextoUbicacion.getText().equals("Volver al menú")){
                    findViewById(R.id.separador_imagen).setVisibility(View.VISIBLE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.GONE);
                    tvTextoUbicacion.setText("Ubicación");
                    btnEvidenciasIniciales.setVisibility(View.VISIBLE);
                    btnActividades.setVisibility(View.VISIBLE);
                    btnFirma.setVisibility(View.VISIBLE);
                    btnEvidenciasFinales.setVisibility(View.VISIBLE);
                    btnINEFotos.setVisibility(View.VISIBLE);

                    progresoReporte += 20;

                    progressBarReporteSupervisor.setProgress(progresoReporte);

                }

            }
        });

        btnEvidenciasIniciales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(tvFotosInciales.getText().equals("Evidencia Incial")){
                    findViewById(R.id.separador_imagen).setVisibility(View.GONE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.VISIBLE);
                    tvFotosInciales.setText("Volver al menú");
                    cargarFragment(new EvidenciasIniciales());
                    btnMiUbicacion.setVisibility(View.GONE);
                    btnActividades.setVisibility(View.GONE);
                    btnFirma.setVisibility(View.GONE);
                    btnEvidenciasFinales.setVisibility(View.GONE);
                    btnINEFotos.setVisibility(View.GONE);


                }else if(tvFotosInciales.getText().equals("Volver al menú")){
                    findViewById(R.id.separador_imagen).setVisibility(View.VISIBLE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.GONE);
                    tvFotosInciales.setText("Evidencia Incial");
                    btnEvidenciasIniciales.setVisibility(View.VISIBLE);
                    btnMiUbicacion.setVisibility(View.VISIBLE);
                    btnActividades.setVisibility(View.VISIBLE);
                    btnFirma.setVisibility(View.VISIBLE);
                    btnEvidenciasFinales.setVisibility(View.VISIBLE);
                    btnINEFotos.setVisibility(View.VISIBLE);

                    progresoReporte += 20;

                    progressBarReporteSupervisor.setProgress(progresoReporte);



                }

            }
        });

        btnActividades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvActividades.getText().equals("Actividades")){
                    findViewById(R.id.separador_imagen).setVisibility(View.GONE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.VISIBLE);
                    tvActividades.setText("Volver al menú");
                    cargarFragment(new Actividades());
                    btnMiUbicacion.setVisibility(View.GONE);
                    btnEvidenciasIniciales.setVisibility(View.GONE);
                    btnFirma.setVisibility(View.GONE);
                    btnEvidenciasFinales.setVisibility(View.GONE);
                    btnINEFotos.setVisibility(View.GONE);

                }else if(tvActividades.getText().equals("Volver al menú")){
                    findViewById(R.id.separador_imagen).setVisibility(View.VISIBLE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.GONE);
                    tvActividades.setText("Actividades");
                    btnEvidenciasIniciales.setVisibility(View.VISIBLE);
                    btnMiUbicacion.setVisibility(View.VISIBLE);
                    btnActividades.setVisibility(View.VISIBLE);
                    btnFirma.setVisibility(View.VISIBLE);
                    btnEvidenciasFinales.setVisibility(View.VISIBLE);
                    btnINEFotos.setVisibility(View.VISIBLE);

                    progresoReporte += 20;

                    progressBarReporteSupervisor.setProgress(progresoReporte);

                }
            }
        });

        btnFirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvFirma.getText().equals("Firma")){
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.VISIBLE);
                    findViewById(R.id.separador_imagen).setVisibility(View.GONE);
                    tvFirma.setText("Volver al menú");
                    cargarFragment(new Firma());
                    btnMiUbicacion.setVisibility(View.GONE);
                    btnEvidenciasIniciales.setVisibility(View.GONE);
                    btnActividades.setVisibility(View.GONE);
                    btnEvidenciasFinales.setVisibility(View.GONE);
                    btnINEFotos.setVisibility(View.GONE);


                }else if(tvFirma.getText().equals("Volver al menú")){
                    findViewById(R.id.separador_imagen).setVisibility(View.VISIBLE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.GONE);
                    tvFirma.setText("Firma");
                    btnEvidenciasIniciales.setVisibility(View.VISIBLE);
                    btnMiUbicacion.setVisibility(View.VISIBLE);
                    btnActividades.setVisibility(View.VISIBLE);
                    btnFirma.setVisibility(View.VISIBLE);
                    btnEvidenciasFinales.setVisibility(View.VISIBLE);
                    btnINEFotos.setVisibility(View.VISIBLE);

                    progresoReporte += 20;

                    progressBarReporteSupervisor.setProgress(progresoReporte);
                }
            }
        });

        btnINEFotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvINEFotos.getText().equals("Fotos Credencial")){
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.VISIBLE);
                    findViewById(R.id.separador_imagen).setVisibility(View.GONE);
                    tvINEFotos.setText("Volver al menú");
                    cargarFragment(new INE());
                    btnMiUbicacion.setVisibility(View.GONE);
                    btnEvidenciasIniciales.setVisibility(View.GONE);
                    btnActividades.setVisibility(View.GONE);
                    btnEvidenciasFinales.setVisibility(View.GONE);
                    btnFirma.setVisibility(View.GONE);


                }else if(tvINEFotos.getText().equals("Volver al menú")){
                    findViewById(R.id.separador_imagen).setVisibility(View.VISIBLE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.GONE);
                    tvINEFotos.setText("Fotos Credencial");
                    btnEvidenciasIniciales.setVisibility(View.VISIBLE);
                    btnMiUbicacion.setVisibility(View.VISIBLE);
                    btnActividades.setVisibility(View.VISIBLE);
                    btnFirma.setVisibility(View.VISIBLE);
                    btnEvidenciasFinales.setVisibility(View.VISIBLE);
                    btnINEFotos.setVisibility(View.VISIBLE);

                    progresoReporte += 20;

                    progressBarReporteSupervisor.setProgress(progresoReporte);

                }
            }
        });

        btnEvidenciasFinales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvFotosFinales.getText().equals("Evidencia Final")){
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.VISIBLE);
                    findViewById(R.id.separador_imagen).setVisibility(View.GONE);
                    tvFotosFinales.setText("Volver al menú");
                    cargarFragment(new EvidenciasFinales());
                    btnMiUbicacion.setVisibility(View.GONE);
                    btnActividades.setVisibility(View.GONE);
                    btnFirma.setVisibility(View.GONE);
                    btnEvidenciasIniciales.setVisibility(View.GONE);
                    btnINEFotos.setVisibility(View.GONE);

                }else if(tvFotosFinales.getText().equals("Volver al menú")){
                    findViewById(R.id.separador_imagen).setVisibility(View.VISIBLE);
                    findViewById(R.id.contenedorProcesoRealizarReporte).setVisibility(View.GONE);
                    tvFotosFinales.setText("Evidencia Final");
                    btnEvidenciasIniciales.setVisibility(View.VISIBLE);
                    btnMiUbicacion.setVisibility(View.VISIBLE);
                    btnActividades.setVisibility(View.VISIBLE);
                    btnFirma.setVisibility(View.VISIBLE);
                    btnEvidenciasFinales.setVisibility(View.VISIBLE);
                    btnINEFotos.setVisibility(View.VISIBLE);

                    progresoReporte += 20;

                    progressBarReporteSupervisor.setProgress(progresoReporte);

                }
            }
        });


        botonFlotante.setOnClickListener(this);

        btnSubirRepote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constantes.OBJETO_REPORTE.validarAtributos()){
                    Toasty.warning(getApplicationContext(),"Algún módulo sin completar", Toast.LENGTH_LONG, true).show();

                }else{
                    Reporte reporte = Constantes.OBJETO_REPORTE;
                    actualizarDatosReporteVacioCreado(reporte);

                }

            }
        });


    }

    protected void cargarFragment(Fragment fragment) {
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.contenedorProcesoRealizarReporte, fragment);
        t.commit();
    }

    private void inicializarElementos() {
        tvTextoUbicacion = findViewById(R.id.tvTextoUbicacion);
        tvFotosInciales = findViewById(R.id.tvTextoFotosIniciales);
        tvActividades = findViewById(R.id.tvTextoActividades);
        tvFirma = findViewById(R.id.tvFirma);
        tvINEFotos = findViewById(R.id.tvTextoFotosINE);
        tvFotosFinales = findViewById(R.id.tvTextoFotosFinales);
        btnMiUbicacion = findViewById(R.id.btnMiUbicacionSupervisor);
        btnEvidenciasIniciales = findViewById(R.id.btnEvidenciasIncialesSupervisor);
        btnActividades = findViewById(R.id.btnActividadesSupervisor);
        btnFirma = findViewById(R.id.btnFirmaSupervisor);
        btnINEFotos = findViewById(R.id.btnFotosINESupervisor);
        btnEvidenciasFinales = findViewById(R.id.btnEvidenciasFinalesSupervisor);
        btnSubirRepote= findViewById(R.id.btnSubirReporteSupervisor);
        progressBarReporteSupervisor = findViewById(R.id.progressBarReporteSupervisor);
        progressBarReporteSupervisor.setProgress(0);
        scrollReporte = findViewById(R.id.scrollMenuRealizarReporteSupervisor);
        botonFlotante =  findViewById(R.id.botonFlotanteSupervisor);
        botonFlotante.setButtonsMenu(R.menu.menu_boton_flotanten_supervisor);

        requestQueue = Volley.newRequestQueue(RealizarReporte.this);

        storage = FirebaseStorage.getInstance();
        progressDialog = new ProgressDialog(RealizarReporte.this);



    }

    //MENUÚ FLOTANTE
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flotante_actividad_extra_supervisor:
                dialogoActividadExtra();

                Toasty.warning(getApplicationContext(),"Agregar actividad extra", Toast.LENGTH_SHORT, true).show();
                break;

            case R.id.flotante_incidencias_supervisor:
                dialogoReportarIncidencia();

                Toasty.warning(getApplicationContext(),"Reportar Incidencia", Toast.LENGTH_SHORT, true).show();
                break;


        }

    }

    private void dialogoReportarIncidencia() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(RealizarReporte.this);
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialogo_reportar_incidencia, null);

        builder.setView(viewDialog);

        final CheckBox cNegocioCerrado = viewDialog.findViewById(R.id.chkNegocioCerrado);
        final CheckBox cDireeccionErronea = viewDialog.findViewById(R.id.chkDireccionErronea);
        final CheckBox cDireccionNoExiste = viewDialog.findViewById(R.id.chkDireccionNoExiste);
        final CheckBox cCambioEquipo = viewDialog.findViewById(R.id.chkCambioEquipo);
        final CheckBox cRetiroEquipo = viewDialog.findViewById(R.id.chkRetiroEquipo);
        final CheckBox cCambioResponsable = viewDialog.findViewById(R.id.chkCambioResponsable);
        final CheckBox cClienteConflictivo = viewDialog.findViewById(R.id.chkClienteConflictivo);
        final CheckBox cEquipoInexistente = viewDialog.findViewById(R.id.chkEquipoInexistente);
        final CheckBox cEquipoYaReparado = viewDialog.findViewById(R.id.chkEquipoReparado);
        final CheckBox cRequiereComodato = viewDialog.findViewById(R.id.chkRequiereComodato);
        final CheckBox cModeloIncorrecto = viewDialog.findViewById(R.id.chkModeloIncorrecto);
        final CheckBox cDificultaInstalacion = viewDialog.findViewById(R.id.chkDificultadInstalacion);
        final CheckBox cDificultadRetiro = viewDialog.findViewById(R.id.chkDificultadRetiro);
        final CheckBox cEquipoRetirado = viewDialog.findViewById(R.id.chkEquipoRetirado);
        final CheckBox cNoRequiereEquipo = viewDialog.findViewById(R.id.chkNoRequiereEquipo);
        final CheckBox cNoEncuentraResponsable = viewDialog.findViewById(R.id.chkNoResponsable);
        final CheckBox cSolicitaVisitaVendedor = viewDialog.findViewById(R.id.chkVisitaVendedor);
        final CheckBox cOtro = viewDialog.findViewById(R.id.chkOtro);

        EditText cajaObservacionesFinales = viewDialog.findViewById(R.id.cajaObservacionesFinalesDeCampo);
        Button btnEnviarIncidencia = viewDialog.findViewById(R.id.btnEnviarIncidencia);



        btnEnviarIncidencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listaIncidencias.clear();

                verificarCheckBoxs(cNegocioCerrado);
                verificarCheckBoxs(cDireeccionErronea);
                verificarCheckBoxs(cDireccionNoExiste);
                verificarCheckBoxs(cCambioEquipo);
                verificarCheckBoxs(cRetiroEquipo);
                verificarCheckBoxs(cCambioResponsable);
                verificarCheckBoxs(cClienteConflictivo);
                verificarCheckBoxs(cEquipoInexistente);
                verificarCheckBoxs(cEquipoYaReparado);
                verificarCheckBoxs(cRequiereComodato);
                verificarCheckBoxs(cModeloIncorrecto);
                verificarCheckBoxs(cDificultaInstalacion);
                verificarCheckBoxs(cDificultadRetiro);
                verificarCheckBoxs(cEquipoRetirado);
                verificarCheckBoxs(cNoRequiereEquipo);
                verificarCheckBoxs(cNoEncuentraResponsable);
                verificarCheckBoxs(cSolicitaVisitaVendedor);
                verificarCheckBoxs(cOtro);

                enviarIncidencias();



            }
        });

        dialogIncidencias = builder.create();


        dialogIncidencias.show();

    }

    private void enviarIncidencias() {
        try {
            for (int i = 0; i < listaIncidencias.size(); i++) {
                String url = Constantes.URL_PRINCIPAL + "insertar_incidencia.php?motivo=" + listaIncidencias.get(i)+ "&fkreporte=" + Constantes.CLAVE_REPORTE_VACIO_CREADO;
                url = url.replace(" ", "%20");

                stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toasty.success(getApplicationContext(), "Infome de incidencias enviado!", Toast.LENGTH_LONG, true);

                        dialogIncidencias.dismiss();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });

                requestQueue.add(stringRequest);
            }

        }catch (Exception e){
            Toasty.error(getApplicationContext(), "Se produjo un problema a la hora de enviar el informe de incidencias.", Toast.LENGTH_LONG, true);

        }

    }

    private void actualizarDatosReporteVacioCreado(Reporte objetoReporte) {

            String url = Constantes.URL_PRINCIPAL + "actualizar_reporte.php?rep_folio=" + Constantes.OBJETO_REPORTE.getOrden() + "&rep_direccion=" + Constantes.OBJETO_REPORTE.getDireccion() + "&rep_latitud=" + Constantes.OBJETO_REPORTE.getLatitud() + "&rep_longitud=" + Constantes.OBJETO_REPORTE.getLongitud() + "&rep_evidencia_inicial_uno=" + Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_uno() + "&rep_evidencia_inicial_dos=" + Constantes.OBJETO_REPORTE.getFoto_evidencia_inicial_dos() + "&rep_evidencia_final_uno=" + Constantes.OBJETO_REPORTE.getFoto_evidencia_final_uno() + "&rep_evidencia_final_dos=" + Constantes.OBJETO_REPORTE.getFoto_evidencia_final_dos() + "&rep_firmavobo=" + Constantes.OBJETO_REPORTE.getFirmavobo() + "&rep_ine_anverso=" + Constantes.OBJETO_REPORTE.getFoto_ine_anverso() + "&rep_ine_reverso=" + Constantes.OBJETO_REPORTE.getFoto_ine_reverso() + "&fkorden=" + Constantes.OBJETO_REPORTE.getClave() + "&orden_creada=" + Constantes.CLAVE_REPORTE_VACIO_CREADO;
            url = url.replace(" ", "%20");

            stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Toasty.success(getApplicationContext(), "Reporte realizado correctamente!", Toast.LENGTH_LONG, true);
                    actualizarEstatusOrdenServicio();
                    actualizarEstatusOrdenOffline();
                    Constantes.OBJETO_REPORTE.limpiarDatosReporte();
                    startActivity(new Intent(RealizarReporte.this, Supervisor.class));

                    finish();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toasty.error(getApplicationContext(), "Se produjo un problema a la hora de realizar el registro. ", Toast.LENGTH_LONG, true);
                }
            });

            requestQueue.add(stringRequest);


    }

    private void crearReporteVacio(final String clave_orden) {

        String url= Constantes.URL_PRINCIPAL +"crear_reporte_vacio.php?clave_orden="+clave_orden;

        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Constantes.CLAVE_REPORTE_VACIO_CREADO =clave_orden;

                //Toasty.custom(RealizarReporte.this,"Reporte listo para ser llenado!",R.drawable.ic_reporte_hojas_icono,R.color.colorPrimaryDark,Toast.LENGTH_LONG,true,true).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        requestQueue.add(stringRequest);
    }

    private void registrarActividadExtra(final String uriFinal, String descripcionActiExtra, String clave) {
        String url=Constantes.URL_PRINCIPAL +"insertar_act_extra.php?descripcion="+descripcionActiExtra+"&fkrep="+Constantes.CLAVE_REPORTE_VACIO_CREADO+"&evidenciafoto="+(uriFinal.replace("%", "%25").replace("&", "%26"));

        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toasty.custom(RealizarReporte.this,"¡Actividad Extra Registrada!",R.drawable.ic_subir_icono,R.color.colorPrimaryDark,Toasty.LENGTH_LONG,true,true).show();

                cajaDescripcionActividad.setText("");
                btnImagenActividadExtra.setVisibility(View.VISIBLE);
                vistaImagenActividadExtra.setVisibility(View.GONE);
                uriFinalActExtra=null;
                uriImagenActividadExtra=null;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toasty.error(RealizarReporte.this, "Se produjo un problema a la hora de subir la actividad extra, intente más tarde...", Toasty.LENGTH_LONG, true);
            }
        });

        requestQueue.add(stringRequest);

    }


    private void dialogoActividadExtra() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(RealizarReporte.this);
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialogo_actividad_extra, null);

        builder.setView(viewDialog);

        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarActividadExtra);
        cajaDescripcionActividad = viewDialog.findViewById(R.id.cajitaDescripcionActividadExtra);

         btnImagenActividadExtra = viewDialog.findViewById(R.id.btnImagenActividadExtra);
         vistaImagenActividadExtra =viewDialog.findViewById(R.id.vistaImagenActividadExtra);

        Button btnEnviarEvidenciaExtra = viewDialog.findViewById(R.id.btnEnviarActividadExtra);

        final AlertDialog dialog = builder.create();

        btnImagenActividadExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cajaDescripcionActividad.getText().toString().isEmpty()){
                    Toasty.warning(RealizarReporte.this,"Describa su actividad",Toasty.LENGTH_LONG,true).show();
                }else{
                    descripcionActiExtra=cajaDescripcionActividad.getText().toString();
                    obtenerFoto(1);
                }

            }
        });


        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnEnviarEvidenciaExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarActividadExtra(uriFinalActExtra,descripcionActiExtra,Constantes.CLAVE_REPORTE_VACIO_CREADO);

            }
        });
        dialog.show();

    }

    private void obtenerFoto(final int num_foto) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(RealizarReporte.this );
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_tipo_evidencia, null);

        builder.setView(viewDialog);

        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarTipoEvidenciaFotografica);

        ImageView btnArchivos = viewDialog.findViewById(R.id.btnSeleccionarFotoOpcion);
        ImageView btnFotoCamara = viewDialog.findViewById(R.id.btnTomarFotoOpcion);


        final AlertDialog dialog = builder.create();

        btnArchivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=1;

                verificarPermisosCamaraMemoria(num_foto, 1);
                dialog.dismiss();


            }
        });

        btnFotoCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoTipoFotoSeleccion=2;

                verificarPermisosCamaraMemoria(num_foto, 2);

                dialog.dismiss();

            }
        });


        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    private void verificarPermisosCamaraMemoria(int num_foto, int tipoCaso) {
        if (RealizarReporte.this != null){
            if (ActivityCompat.checkSelfPermission(RealizarReporte.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RealizarReporte.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(RealizarReporte.this , new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
                ActivityCompat.requestPermissions(RealizarReporte.this , new String[]{Manifest.permission.CAMERA}, 3);
            } else {
                abrirCamara(num_foto,tipoCaso);

            }
        }


    }

    private void abrirCamara(int foto, int tipoCaso) {

        if (foto == 1 && tipoCaso == 1) {
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_LOCAL_ONLY, uriImagenActividadExtra);
            startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), SOLICITUD_FOTO_ACT_EXTRA);

        } else if(foto==1 && tipoCaso==2){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
            String currentDateandTime = sdf.format(new Date());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "act_extra_"+currentDateandTime+".jpg");

            if (RealizarReporte.this != null) {
                uriImagenActividadExtra = FileProvider.getUriForFile(RealizarReporte.this, BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenActividadExtra);
                startActivityForResult(intent, SOLICITUD_FOTO_ACT_EXTRA);
            }else{
                Toast.makeText(RealizarReporte.this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //1 = SELECCIÓN DE ARCHIVOS
        //2 = TOMAR FOTO DESDE LA CÁMARA

        if (requestCode == SOLICITUD_FOTO_ACT_EXTRA && resultCode == RESULT_OK &&casoTipoFotoSeleccion==1) {
            try {
                Uri u = data.getData();
                Bitmap bitmap = decodificarBitmap(RealizarReporte.this, u);

                btnImagenActividadExtra.setVisibility(View.GONE);
                vistaImagenActividadExtra.setVisibility(View.VISIBLE);
                vistaImagenActividadExtra.setImageBitmap(bitmap);


                subirFotoFirebase(u,SOLICITUD_FOTO_ACT_EXTRA,bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if(requestCode == SOLICITUD_FOTO_ACT_EXTRA && resultCode == RESULT_OK &&casoTipoFotoSeleccion==2){
            try {

                Bitmap bitmap = decodificarBitmap(RealizarReporte.this, uriImagenActividadExtra);

                btnImagenActividadExtra.setVisibility(View.GONE);
                vistaImagenActividadExtra.setVisibility(View.VISIBLE);
                vistaImagenActividadExtra.setImageBitmap(bitmap);

                subirFotoFirebase(uriImagenActividadExtra,SOLICITUD_FOTO_ACT_EXTRA,bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
        int anchoMarco = 600;
        int altoMarco = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int fotoAncho = bmOptions.outWidth;
        int fotoAlto = bmOptions.outHeight;

        int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = escalaImagen;

        return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
    }

    void subirFotoFirebase(Uri uri, final int numFoto, Bitmap foto) {

        storageReference = storage.getReference("reporte_" + Constantes.OBJETO_REPORTE.getClave());//FOTOS SEGÚN SEA EL CASO

        switch (numFoto) {
            case SOLICITUD_FOTO_ACT_EXTRA:

                procesoSubidaFirebase(uri, SOLICITUD_FOTO_ACT_EXTRA, foto);

                break;
        }
    }

    private byte[] getImageCompressed(Bitmap photo) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        return baos.toByteArray();
    }

    private void procesoSubidaFirebase(Uri uri, final int numFoto, Bitmap foto) {
        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());

        final byte[] data = getImageCompressed(foto);

        UploadTask uploadTask = fotoReferencia.putBytes(data);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();

                }

                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                String uriFinal = task.getResult().toString();

                uriFinalActExtra = uriFinal;


                progressDialog.hide();
                Toasty.success(getApplicationContext(),"Evidencia lista para ser enviada!", Toast.LENGTH_LONG, true).show();
            }

        });
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                double cargado = (taskSnapshot.getBytesTransferred()/1024);
                double total = (taskSnapshot.getTotalByteCount()/1024);
                int cargaActual = (int) cargado;


                progressDialog.setTitle("Subiendo tu foto");
                progressDialog.setMessage(((int)progress) + "% subido...");
                progressDialog.setCancelable(false);
                progressDialog.show();


                System.out.println("Upload is "+progress+"% done");
            }
        });
    }

    private void actualizarEstatusOrdenServicio(){
        // Estados
        // 0 => Pendiente,
        // 1 => Completado,
        // 2 => Cancelado
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());

        Log.e("Fecha Date", currentDateandTime);
        AsyncHttpClient client = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"actualizar_estatus_orden_servicio.php?estatus=1&clave_orden="+ Constantes.OBJETO_REPORTE.getClave()+"&fecha_finaliza="+currentDateandTime;
        client.post(url,null,new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void verificarCheckBoxs(CheckBox checkBox){
        if (checkBox.isChecked()){
            listaIncidencias.add(checkBox.getText().toString());
            checkBox.setChecked(false);
        }
    }

    private void sincronizarSQL(){
        sqlHelper = new SQLHelper(this,SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());

        try {
            String insert = "INSERT INTO "+Constantes.TABLA_REPORTE_CREADO+" " +
                    "("+Constantes.REPCR_CLAVE+", "+Constantes.REPCR_USUARIO+", "+Constantes.REPCR_FOLIO+", "+Constantes.REPCR_DIRECCION+", "+Constantes.REPCR_LATITUD+", "
                    +Constantes.REPCR_LONGITUD+", "+Constantes.REPCR_FOTO_INICIAL_1+", "+Constantes.REPCR_FOTO_INICIAL_2+", "+Constantes.REPCR_FOTO_FINAL_1+", "+Constantes.REPCR_FOTO_FINAL_2+", " +
                    Constantes.REPCR_FIRMA+ ", "+Constantes.REPCR_FOTO_INE_1+", "+Constantes.REPCR_FOTO_INE_2+", "+Constantes.REPCR_ORDEN+", "+Constantes.RPPCR_FECHA_BD_LOCAL+", "+Constantes.RPPCR_ESTADO_ENVIO+") "+
                    "VALUES ('"+Constantes.OBJETO_REPORTE.getClave()+"', '','','','','','','','','','','','','','"+currentDateandTime+"','0') "; //El último String es para mantener en 0 si no se ha enviado y en 1 si ya se envió

            db.execSQL(insert);
            db.close();
        }catch (Exception e){}
    }
    private void actualizarEstatusOrdenOffline() {
        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        String insert =
                "UPDATE " + Constantes.TABLA_REPORTE_CREADO + " " +
                        "SET " + Constantes.RPPCR_ESTADO_ENVIO + "= '1' " +
                        "WHERE " + Constantes.REPCR_CLAVE + " = '" + Constantes.OBJETO_REPORTE.getClave() + "'";

        db.execSQL(insert);
        db.close();
    }

    private void consultarPendientes() {

        try {
            SQLiteDatabase db = sqlHelper.getReadableDatabase();
            String[] parametros = {Constantes.OBJETO_REPORTE.getClave()};

            Cursor cursor = db.rawQuery("SELECT * FROM "+Constantes.TABLA_REPORTE_CREADO+
                            " WHERE "+Constantes.REPCR_CLAVE+" = ? ", parametros);
            if (cursor.getCount() > 0){

                while (cursor.moveToNext()){
                    String clave = cursor.getString(0);
                    String usuario = cursor.getString(1);
                    String folio   = cursor.getString(2);
                    String direccion= cursor.getString(3);
                    String latitud  = cursor.getString(4);
                    String longitud = cursor.getString(5);
                    String fotoinicial1 = cursor.getString(6);
                    String fotoinicial2 = cursor.getString(7);
                    String fotofinal1 = cursor.getString(8);
                    String fotofinal2 = cursor.getString(9);

                    String fotoFirma  = cursor.getString(10);
                    String fotoIne1   = cursor.getString(11);
                    String fotoIne2   = cursor.getString(12);
                    String orden      = cursor.getString(13);
                    String fechalocal = cursor.getString(14);
                    String estadoenvio= cursor.getString(15);
                    //if (cursor.getCount()>1){


                    //Estado = 0 //Sin enviar (Se crea por default)
                    //Estado = 1 //Enviado
                    if (estadoenvio.contains("0"))
                    {
                        Toasty.warning(getApplicationContext(), "1",Toasty.LENGTH_LONG).show();
                        //Método para enviar las fotos hacer metodo para enviar

                        envioUriFotosPendientes(clave,fotoinicial1,fotoinicial2, fotofinal1,fotofinal2,fotoIne1, fotoIne2, fotoFirma);

                        dialogReporteSinConexion(folio,direccion,latitud,longitud,fotoinicial1,fotoinicial2,fotofinal1, fotofinal2,fotoFirma, fotoIne1, fotoIne2, orden);
                    }else{
                    }
                }
            }
        }catch (Exception e){ }
    }


    private void envioUriFotosPendientes(String clave, String fotoinicial1, String fotoinicial2,
                                      String fotofinal1, String fotofinal2,
                                      String fotoine1, String fotoine2, String fotofirma ) {
        //Aquí enviaría los elements (?)
        //Una vez enviados se actualizará a la base de datos. Así se cambia el layout.
        //Ya no es necesario mostrar lo de bd, por que se consume de ws
        storageReference = storage.getReference("reporte_" + clave);

        try {

            Uri foto1 = Uri.parse(fotoinicial1);
            Bitmap bitmap = decodificarBitmap(getApplicationContext(), foto1);


            Uri foto2 = Uri.parse(fotoinicial2);
            Bitmap bitmap1 = decodificarBitmap(getApplicationContext(), foto2);

            Uri foto3 = Uri.parse(fotofinal1);
            Bitmap bitmap2 = decodificarBitmap(getApplicationContext(), foto3);

            Uri foto4 = Uri.parse(fotofinal2);
            Bitmap bitmap3 = decodificarBitmap(getApplicationContext(), foto4);

            Uri foto5 = Uri.parse(fotoine1);
            Bitmap bitmap4 = decodificarBitmap(getApplicationContext(), foto5);

            Uri foto6 = Uri.parse(fotoine2);
            Bitmap bitmap5 = decodificarBitmap(getApplicationContext(), foto6);

            Uri foto7 = Uri.parse(fotofirma);
            Bitmap bitmap6 = decodificarBitmap(getApplicationContext(), foto7);


            subirFotoFirebasePendientes(foto1, EvidenciasIniciales.SOLICITUD_FOTO_INICIAL_1, bitmap, clave);
            subirFotoFirebasePendientes(foto2, EvidenciasIniciales.SOLICITUD_FOTO_INICIAL_2,bitmap1, clave);
            subirFotoFirebasePendientes(foto3, EvidenciasFinales.SOLICITUD_FOTO_FINAL_1, bitmap2, clave);
            subirFotoFirebasePendientes(foto4, EvidenciasFinales.SOLICITUD_FOTO_FINAL_2, bitmap3, clave);
            subirFotoFirebasePendientes(foto5, INE.SOLICITUD_INE_ANVERSO, bitmap4, clave);
            subirFotoFirebasePendientes(foto6, INE.SOLICITUD_INE_REVERSO, bitmap5, clave);
            subirFotoFirebasePendientes(foto7, Firma.SOLICITUD_FIRMA, bitmap6, clave);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void enviarPendientes(String folio, String direccion, String latitud,
                                  String longitud, String fotoinicial1, String fotoinicial2,
                                  String fotofinal1, String fotofinal2, String fotoFirma, String fotoIne1,
                                  String fotoIne2, String orden){

        /*if (!fotoinicial1.isEmpty() ){
            Uri uri = Uri.parse(fotoinicial1);
            decodificaEnviaUrl(uri);
        }*/

        String url = Constantes.URL_PRINCIPAL + "actualizar_reporte.php?" +
                "rep_folio=" + folio +
                "&rep_direccion=" + direccion +
                "&rep_latitud=" + latitud +
                "&rep_longitud=" + longitud +
                "&rep_evidencia_inicial_uno=" + fotoinicial1 +
                "&rep_evidencia_inicial_dos=" + fotoinicial2 +
                "&rep_evidencia_final_uno=" + fotofinal1 +
                "&rep_evidencia_final_dos=" + fotofinal2 +
                "&rep_firmavobo=" + fotoFirma +
                "&rep_ine_anverso=" + fotoIne1 +
                "&rep_ine_reverso=" + fotoIne2 +
                "&fkorden=" + orden +
                "&orden_creada=" + Constantes.CLAVE_REPORTE_VACIO_CREADO;
        url = url.replace(" ", "%20");

        stringRequest = new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {

                Toasty.success(getApplicationContext(), "Reporte realizado correctamente!", Toast.LENGTH_LONG, true);
                actualizarEstatusOrdenServicio();
                actualizarEstatusOrdenOffline();
                dialogReporte.dismiss();

                //Constantes.OBJETO_REPORTE.limpiarDatosReporte();
                startActivity(new Intent(RealizarReporte.this, Supervisor.class));
                finish();

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toasty.error(getApplicationContext(), "Se produjo un problema a la hora de realizar el registro. ", Toast.LENGTH_LONG, true);
            }
        });
        requestQueue.add(stringRequest);


    }


    private void dialogReporteSinConexion(final String folio, final String direccion, final String latitud,
                                          final String longitud, final String fotoinicial1, final String fotoinicial2,
                                          final String fotofinal1, final String fotofinal2, final String fotoFirma, final String fotoIne1,
                                          final String fotoIne2, final String orden) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_reporte_sin_conexion, null);

        builder.setView(viewDialog);

        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarSC);
        Button btnEnviar = viewDialog.findViewById(R.id.btnEnviarSC);

        dialogReporte = builder.create();

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enviarPendientes(folio, direccion,latitud,longitud,fotoinicial1,fotoinicial2,fotofinal1,fotofinal2,fotoFirma,fotoIne1,fotoIne2,orden);
            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogReporte.dismiss();
            }
        });
        dialogReporte.show();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    private void subirFotoFirebasePendientes(Uri uri, final int numFoto, Bitmap foto, final String clave) {
        final StorageReference fotoReferencia = storageReference.child(uri.getLastPathSegment());

        final byte[] data = getImageCompressed(foto);
        UploadTask uploadTask = fotoReferencia.putBytes(data);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return fotoReferencia.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task)
            {
                String uriFinal = task.getResult().toString();

                switch (numFoto){
                    case EvidenciasIniciales.SOLICITUD_FOTO_INICIAL_1:
                        //
                        SQLiteDatabase db = sqlHelper.getWritableDatabase();

                        String insert =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                        "SET "+Constantes.REPCR_FOTO_INICIAL_1+"= '"+uriFinal+"' " +
                        "WHERE "+Constantes.REPCR_CLAVE+" = '"+clave+"'";

                        db.execSQL(insert);
                        break;

                    case EvidenciasIniciales.SOLICITUD_FOTO_INICIAL_2:
                        db = sqlHelper.getWritableDatabase();

                        String insert1 =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                        "SET "+Constantes.REPCR_FOTO_INICIAL_2+"= '"+uriFinal+"' " +
                        "WHERE "+Constantes.REPCR_CLAVE+" = '"+clave+"'";

                        db.execSQL(insert1);
                        break;

                    case EvidenciasFinales.SOLICITUD_FOTO_FINAL_1:
                        //
                        db = sqlHelper.getWritableDatabase();

                        String insert2 =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                        "SET "+Constantes.REPCR_FOTO_FINAL_1+"= '"+uriFinal+"' " +
                        "WHERE "+Constantes.REPCR_CLAVE+" = '"+clave+"'";

                        db.execSQL(insert2);
                        break;

                    case EvidenciasFinales.SOLICITUD_FOTO_FINAL_2:
                        db = sqlHelper.getWritableDatabase();

                        String insert3 =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                        "SET "+Constantes.REPCR_FOTO_FINAL_2+"= '"+uriFinal+"' " +
                        "WHERE "+Constantes.REPCR_CLAVE+" = '"+clave+"'";

                        db.execSQL(insert3);
                        break;

                    case Firma.SOLICITUD_FIRMA:
                        //procesoSubidaFirebasePendientes(uri, Firma.SOLICITUD_FIRMA,foto);
                        db = sqlHelper.getWritableDatabase();

                        String insert4 =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                        "SET "+Constantes.REPCR_FIRMA+"= '"+uriFinal+"' " +
                        "WHERE "+Constantes.REPCR_CLAVE+" = '"+clave+"'";

                        db.execSQL(insert4);
                        break;

                    case INE.SOLICITUD_INE_ANVERSO:
                        //
                        db = sqlHelper.getWritableDatabase();

                        String insert6 =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                        "SET "+Constantes.REPCR_FOTO_INE_1+"= '"+uriFinal+"' " +
                        "WHERE "+Constantes.REPCR_CLAVE+" = '"+clave+"'";

                        db.execSQL(insert6);
                        break;

                    case INE.SOLICITUD_INE_REVERSO:
                        //
                        db = sqlHelper.getWritableDatabase();

                        String insert7 =
                        "UPDATE "+Constantes.TABLA_REPORTE_CREADO+" " +
                        "SET "+Constantes.REPCR_FOTO_INE_2+"= '"+uriFinal+"' " +
                        "WHERE "+Constantes.REPCR_CLAVE+" = '"+clave+"'";

                        db.execSQL(insert7);
                        break;
                }
            }

        });

    }
}
