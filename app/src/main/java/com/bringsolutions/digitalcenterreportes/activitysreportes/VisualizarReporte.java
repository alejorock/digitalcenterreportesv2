package com.bringsolutions.digitalcenterreportes.activitysreportes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.herramientas.ZoomImageView;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class VisualizarReporte extends AppCompatActivity {
    String claveOrdenSeleccionada;

    private TextView tvDetalleFolio, tvDetalleDireccion;
    private ImageView imgDetalleInicialUno, imgDetalleInicialDos, imgDetalleFinalUno,
            imgDetalleFinalDos, imgDetalleFirma, imgDetalleIneUno, imgDetalleIneDos;

    LinearLayout lnContenidoDetalle, lnCargaDetalle;

    String fotoInicialUno, fotoInicialDos, fotoFinalUno, fotoFinalDos, fotoINE_Frente, fotoINE_Atras, fotoFirma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_reporte);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intentRecibir= getIntent();
        String claveOrdenRecibida = intentRecibir.getStringExtra("ID_CLAVE_ORDEN");
        claveOrdenSeleccionada= claveOrdenRecibida;

        getSupportActionBar().setTitle("Visualizar Reporte : " + claveOrdenRecibida);

        tvDetalleFolio = findViewById(R.id.tvDetalleFolio);
        tvDetalleDireccion = findViewById(R.id.tvDetalleDireccion);
        imgDetalleInicialUno = findViewById(R.id.imgDetalleInicialUno);
        imgDetalleInicialDos = findViewById(R.id.imgDetalleInicialDos);
        imgDetalleFinalUno = findViewById(R.id.imgDetalleFinalUno);
        imgDetalleFinalDos = findViewById(R.id.imgDetalleFinalDos);
        imgDetalleFirma    = findViewById(R.id.imgDetalleFirma);
        imgDetalleIneUno   = findViewById(R.id.imgDetalleINEUno);
        imgDetalleIneDos   = findViewById(R.id.imgDetalleINEDos);

        lnCargaDetalle = findViewById(R.id.lnLoadingDetalle);
        lnContenidoDetalle = findViewById(R.id.lnContenidoDetalle);

        cargaDatosVistas();

        clicks();
    }

    private void clicks() {
        imgDetalleInicialUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(VisualizarReporte.this, ZoomImageView.class);
                ii.putExtra("foto_inicial_uno_re", fotoInicialUno);
                startActivity(ii);


            }
        });

        imgDetalleInicialDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(VisualizarReporte.this, ZoomImageView.class);
                ii.putExtra("foto_inicial_dos_re", fotoInicialDos);
                startActivity(ii);


            }
        });

        imgDetalleFinalUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(VisualizarReporte.this, ZoomImageView.class);
                ii.putExtra("foto_final_uno_re", fotoFinalUno);
                startActivity(ii);
            }
        });

        imgDetalleFinalDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(VisualizarReporte.this, ZoomImageView.class);
                ii.putExtra("foto_final_dos_re", fotoFinalDos);
                startActivity(ii);
            }
        });

        imgDetalleFirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(VisualizarReporte.this, ZoomImageView.class);
                ii.putExtra("foto_firma_re", fotoFirma);
                startActivity(ii);

            }
        });

        imgDetalleIneUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ii = new Intent(VisualizarReporte.this, ZoomImageView.class);
                ii.putExtra("ine_frente_re", fotoINE_Frente);
                startActivity(ii);

            }
        });

        imgDetalleIneDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ii = new Intent(VisualizarReporte.this, ZoomImageView.class);
                ii.putExtra("ine_atras_re", fotoINE_Atras);
                startActivity(ii);

            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    private void cargaDatosVistas() {
        cargarWSDatos();
    }


    private void cargarWSDatos() {
        lnContenidoDetalle.setVisibility(View.GONE);
        lnCargaDetalle.setVisibility(View.VISIBLE);

        AsyncHttpClient cliente = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"mostrar_reportes_id.php?id="+ claveOrdenSeleccionada;
        cliente.get(url, null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    obtieneJSON(new String(responseBody));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //Manejo de error
                Log.e("Error conexión", error.getLocalizedMessage());
            }
        });
    }
    private void obtieneJSON(String response) {
        try{
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length();i++){
                JSONObject js = jsonArray.getJSONObject(i);
                //String fkusuario = js.getString("rep_fkusuario");
                String foliov     = js.getString("rep_folio");
                String direccionv = js.getString("rep_direccion");
                String latitudv   = js.getString("rep_latitud");
                String longitudv  = js.getString("rep_longitud");
                String evidencia_incial_1v = js.getString("rep_evidencia_inicial_uno");
                String evidencia_incial_2v = js.getString("rep_evidencia_inicial_dos");
                String evidencia_final_1v  = js.getString("rep_evidencia_final_uno");
                String evidencia_final_2v  = js.getString( "rep_evidencia_final_dos");
                String firmav              = js.getString("rep_firmavobo");
                String ine_anversov        = js.getString("rep_ine_anverso");
                String ine_reversov        = js.getString("rep_ine_reverso");
                String ordenv              = js.getString("rep_fkorden");

                fotoInicialUno=evidencia_incial_1v;
                fotoInicialDos=evidencia_incial_2v;
                fotoFinalUno=evidencia_final_1v;
                fotoFinalDos=evidencia_final_2v;
                fotoINE_Frente=ine_anversov;
                fotoINE_Atras=ine_reversov;
                fotoFirma=firmav;

                tvDetalleFolio.setText("Folio: "+ foliov);
                tvDetalleDireccion.setText(direccionv);

                Glide.with(VisualizarReporte.this)
                        .load(evidencia_incial_1v)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleInicialUno);

                Glide.with(VisualizarReporte.this)
                        .load(evidencia_incial_2v)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleInicialDos);

                Glide.with(VisualizarReporte.this)
                        .load(evidencia_final_1v)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleFinalUno);
                Glide.with(VisualizarReporte.this)
                        .load(evidencia_final_2v)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleFinalDos);

                Glide.with(VisualizarReporte.this)
                        .load(ine_anversov)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleIneUno);

                Glide.with(VisualizarReporte.this)
                        .load(ine_reversov)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleIneDos);

                Glide.with(VisualizarReporte.this)
                        .load(firmav)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imgDetalleFirma);
                lnContenidoDetalle.setVisibility(View.VISIBLE);
                lnCargaDetalle.setVisibility(View.GONE);

            }
        }catch (Exception e){
            Log.e("Error conexion", e.getLocalizedMessage());
        }
    }
}
