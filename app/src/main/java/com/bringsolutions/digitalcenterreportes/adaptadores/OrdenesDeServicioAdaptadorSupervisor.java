package com.bringsolutions.digitalcenterreportes.adaptadores;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.activitysprincipales.Login;
import com.bringsolutions.digitalcenterreportes.activitysreportes.RealizarReporte;
import com.bringsolutions.digitalcenterreportes.activitysreportes.VisualizarReporte;
import com.bringsolutions.digitalcenterreportes.objetos.Cliente;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.OrdenServicioSupervisor;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;


public class OrdenesDeServicioAdaptadorSupervisor extends RecyclerView.Adapter<OrdenesDeServicioAdaptadorSupervisor.ViewHolder> {
    Cliente cliente;
    TextView tvRegion, tvNoCliente, tvEstablecimiento, tvNombreCliente, tvCalle, tvEntreCalle, tvYcalle, tvNoExterno, tvColonia, tvPoblacion, tvCodigoPostal;

    //clase viewholder para enlazar componesntes con la vista de los mensajes
    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvClave, tvNombre, tvFecha, tvEstatus;
        private CardView tarjetaOrdenServicio;

        public ViewHolder(View itemView) {
            super(itemView);
            //enlanzando elementos
            tvClave = itemView.findViewById(R.id.tarjeta_orden_servicio_CLAVE);
            tvNombre = itemView.findViewById(R.id.tarjeta_orden_servicio_NOMBRE);
            tvFecha = itemView.findViewById(R.id.tarjeta_orden_servicio_FECHA_REGISTRADO);
            tvEstatus = itemView.findViewById(R.id.tarjeta_orden_servicio_ESTATUS);
            tarjetaOrdenServicio = itemView.findViewById(R.id.card_tarjeta_orden_servicio_supervisor);

        }

    }

    public List<OrdenServicioSupervisor> ordenesServicioLista;
    public Context context;


    public OrdenesDeServicioAdaptadorSupervisor(List<OrdenServicioSupervisor> ordenesServicioLista, Context context) {
        this.ordenesServicioLista = ordenesServicioLista;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_orden_servicio,viewGroup,false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {

        viewHolder.tvClave.setText("Clave: "+ordenesServicioLista.get(i).getOs_clave());
        viewHolder.tvNombre.setText(ordenesServicioLista.get(i).getOs_nombre());
        viewHolder.tvFecha.setText("Fecha: "+ordenesServicioLista.get(i).getOs_fechaIngreso());

        if(ordenesServicioLista.get(i).getOs_estatus().equals("0")){
            viewHolder.tvEstatus.setText("Aún no ha sido finalizada");
            viewHolder.tvEstatus.setBackgroundColor(Color.RED);
        }else if(ordenesServicioLista.get(i).getOs_estatus().equals("1")){
            viewHolder.tvEstatus.setText("Finalizada");
            viewHolder.tvEstatus.setBackgroundColor(Color.GREEN);
        }else if(ordenesServicioLista.get(i).getOs_estatus().equals("2")){
            viewHolder.tvEstatus.setText("Reprogramada");
            viewHolder.tvEstatus.setBackgroundColor(Color.rgb(255,90,55));
        }else if(ordenesServicioLista.get(i).getOs_estatus().equals("3")){
            viewHolder.tvEstatus.setText("Aprobada");
            viewHolder.tvEstatus.setBackgroundColor(Color.rgb(9,93,139));

        }

        viewHolder.tarjetaOrdenServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String estatus = ordenesServicioLista.get(i).getOs_estatus();
                String claveOrden = ordenesServicioLista.get(i).getOs_clave();

                if(estatus.equals("0")){
                    //Activity para realizar el reporte
                    Intent in = new Intent(context, RealizarReporte.class);
                    in.putExtra("ID_CLAVE_ORDEN", claveOrden);
                    Constantes.OBJETO_REPORTE.setUsuario(Constantes.USUARIO_INICIO_SESION_LOGIN.getIdUsuario());
                    Constantes.OBJETO_REPORTE.setClave(claveOrden);
                    Constantes.OBJETO_REPORTE.setOrden(ordenesServicioLista.get(i).getOs_noOrden());

                    context.startActivity(in);

                }else if(estatus.equals("1")){
                    //Activity para visualizar el reporte hecho
                    Intent ii = new Intent(context, VisualizarReporte.class);
                    ii.putExtra("ID_CLAVE_ORDEN", claveOrden);
                    context.startActivity(ii);

                }
            }
        });
        viewHolder.tarjetaOrdenServicio.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                lanzarDialogoInfoCliente(ordenesServicioLista.get(i).getOs_clave());
                return true;
            }
        });




    }

    private void lanzarDialogoInfoCliente(String os_clave) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View viewDialog = layoutInflater.inflate(R.layout.dialogo_info_cliente, null);

        builder.setView(viewDialog);


        tvRegion = viewDialog.findViewById(R.id.tvRegionCliente);
        tvNoCliente =  viewDialog.findViewById(R.id.tvNoCliente);
        tvEstablecimiento =  viewDialog.findViewById(R.id.tvEstablecimientoCliente);
        tvNombreCliente =  viewDialog.findViewById(R.id.tvNombreCliente);
        tvCalle =  viewDialog.findViewById(R.id.tvCalleCliente);
        tvEntreCalle =  viewDialog.findViewById(R.id.tvEntreCalleCliente);
        tvYcalle =  viewDialog.findViewById(R.id.tvYcalleCliente);
        tvNoExterno =  viewDialog.findViewById(R.id.tvNoExternoCliente);
        tvColonia =  viewDialog.findViewById(R.id.tvColoniaCliente);
        tvPoblacion =  viewDialog.findViewById(R.id.tvPoblaccion);
        tvCodigoPostal =  viewDialog.findViewById(R.id.tvCPcliente);

        obtenerInfoCliente(os_clave);

        final AlertDialog dialog = builder.create();

        dialog.show();

    }

    private void obtenerInfoCliente(String os_clave) {

        cliente = new Cliente();


        final AsyncHttpClient client = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL+"mostrar_informacion_cliente_x_orden.php?fkorden="+os_clave;

        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){

                    try {
                        JSONArray jsonArray = new JSONArray(new String(responseBody));
                        for (int i=0; i<jsonArray.length();i++){
                            JSONObject js = jsonArray.getJSONObject(i);

                            String os_clave     = js.getString("os_clave");
                            String os_noOrden     = js.getString("os_noOrden");
                            String os_nombre     = js.getString("os_nombre");
                            String os_fechaIngreso     = js.getString("os_fechaIngreso");
                            String os_fechaProgramada     = js.getString("os_fechaProgramada");
                            String os_fechaFinalizacion     = js.getString("os_fechaFinalizacion");
                            String os_fechaReprogramacion     = js.getString("os_fechaReprogramacion");
                            String os_estatus     = js.getString("os_estatus");
                            String os_fkasi     = js.getString("os_fkasi");
                            String os_observaciones     = js.getString("os_observaciones");
                            String os_prioridad     = js.getString("os_prioridad");
                            String asi_clave     = js.getString("asi_clave");
                            String asi_fkest     = js.getString("asi_fkest");
                            String asi_fkusu_supervisor     = js.getString("asi_fkusu_supervisor");
                            String asi_fkveh     = js.getString("asi_fkveh");
                            String est_clave     = js.getString("est_clave");

                            String est_region     = js.getString("est_region");
                            String est_noCliente     = js.getString("est_noCliente");
                            String est_nomCliente     = js.getString("est_nomCliente");
                            String est_nomCliente2     = js.getString("est_nomCliente2");
                            String est_calle     = js.getString("est_calle");
                            String est_entreCalle     = js.getString("est_entreCalle");
                            String est_yCalle     = js.getString("est_yCalle");
                            String est_noExterno     = js.getString("est_noExterno");
                            String est_colonia     = js.getString("est_colonia");
                            String est_poblacion     = js.getString("est_poblacion");
                            String est_cp     = js.getString("est_cp");
                            String est_descripcion     = js.getString("est_descripcion");
                            String est_latitud     = js.getString("est_latitud");
                            String est_longitud     = js.getString("est_descripcion");
                            String est_telefono     = js.getString("est_telefono");
                            String est_fkger     = js.getString("est_fkger");

                            if(est_cp.equals("0") || est_cp.length()<3 || est_cp.equals("") || est_cp.equals("null") || est_cp==null){
                                est_cp="Sin establecer";
                            }

                            if(est_colonia.equals("0") || est_colonia.length()<3 || est_colonia.equals("") || est_colonia.equals("null") || est_colonia==null){
                                est_colonia="Sin establecer";
                            }

                            if(est_yCalle.equals("0") || est_yCalle.length()<3 || est_yCalle.equals("") || est_yCalle.equals("null") || est_yCalle==null){
                                est_yCalle="Sin establecer";
                            }

                            if(est_entreCalle.equals("0") || est_entreCalle.length()<3 || est_entreCalle.equals("") || est_entreCalle.equals("null") || est_entreCalle==null){
                                est_entreCalle="Sin establecer";
                            }



                            tvRegion.setText(est_region);
                            tvNoCliente.setText(est_noCliente);
                            tvEstablecimiento.setText(est_nomCliente);
                            tvNombreCliente.setText(est_nomCliente2);
                            tvCalle.setText(est_calle);
                            tvEntreCalle.setText(est_entreCalle);
                            tvYcalle.setText(est_yCalle);
                            tvNoExterno.setText(est_noExterno);
                            tvColonia.setText(est_colonia);
                            tvPoblacion.setText(est_poblacion);
                            tvCodigoPostal.setText(est_cp);




                        }

                    }catch (Exception e){
                        Toasty.warning(context,"Hubo un problema, intente más tarde.", Toast.LENGTH_LONG, true).show();
                    }


                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toasty.warning(context,"Hubo un problema, intente más tarde.", Toast.LENGTH_LONG, true).show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return ordenesServicioLista.size();
    }



}
