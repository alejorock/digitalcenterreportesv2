package com.bringsolutions.digitalcenterreportes.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.herramientas.UbicarUsuario;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.ObjetoSupervisor;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;
import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class SupervisoresAdaptador extends RecyclerView.Adapter<SupervisoresAdaptador.ViewHolder> {

    //clase viewholder para enlazar componesntes con la vista de los mensajes
    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvNombreSupervisor, tvUsuarioNombre;
        private CardView tarjetaSupervisor;
        private ImageButton btnUbicarSupervisor;

        public ViewHolder(View itemView) {
            super(itemView);
            //enlanzando elementos
            tvNombreSupervisor = itemView.findViewById(R.id.tvNombreSupervisorTarjeta);
            tvUsuarioNombre = itemView.findViewById(R.id.tvNombreUsuarioSupervisorTarjeta);
            tarjetaSupervisor = itemView.findViewById(R.id.tarjeta_supervisor_card);
            btnUbicarSupervisor = itemView.findViewById(R.id.btnUbicarSupervisorTarjeta);

        }

    }

    public List<ObjetoSupervisor> supervisorList;
    public Context context;

    public SupervisoresAdaptador(List<ObjetoSupervisor> supervisorList, Context context) {
        this.supervisorList = supervisorList;
        this.context = context;
    }

    @NonNull
    @Override
    public SupervisoresAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_supervisor,viewGroup,false);

        return new SupervisoresAdaptador.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SupervisoresAdaptador.ViewHolder viewHolder, final  int i) {

        viewHolder.tvNombreSupervisor.setText(supervisorList.get(i).getNombreSupervisor());
        viewHolder.tvUsuarioNombre.setText(supervisorList.get(i).getUsuarioNombreSupervisor());
        viewHolder.btnUbicarSupervisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String ID_SUPERVISOR = supervisorList.get(i).getIdSupervisor();
                obtenerUltimaUbicacion(ID_SUPERVISOR);


            }
        });


    }

    @Override
    public int getItemCount() {
        return supervisorList.size();
    }


    private void obtenerUltimaUbicacion(String id_supervisor) {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL+"obtener_ultima_ubicacion_supervisor.php?id_supervisor="+id_supervisor;

        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toasty.warning(context,"Hubo un problema, intente más tarde.", Toast.LENGTH_LONG, true).show();

            }
        });

    }

    private void obtieneJSON(String response) {
        if(response.length()==3){
            Toasty.warning(context,"Este supervisor no tiene registros de ubicaciones.", Toast.LENGTH_LONG, true).show();
        }
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i=0; i<jsonArray.length();i++){
                JSONObject js = jsonArray.getJSONObject(i);

                final String seg_latitud  = js.getString("seg_latitud");
                final String seg_longitud = js.getString("seg_longitud");
                final String seg_tiempo   = js.getString("seg_tiempo");

                try{
                    Double lati  = Double.parseDouble(seg_latitud);
                    Double longi = Double.parseDouble(seg_longitud);

                    if(lati==0.0  || longi==0.0 || (lati==0.0 && longi==0.0) || lati==null || longi==null || (lati==null && longi==null)){
                        Toasty.warning(context,"Este supervisor no tiene registros de ubicaciones.", Toast.LENGTH_LONG, true).show();
                    }else {
                        Intent ii = new Intent(context, UbicarUsuario.class);
                        ii.putExtra("LATITUD_RECIBIDA_SUPERVISOR", seg_latitud);
                        ii.putExtra("LONGITUD_RECIBIDA_SUPERVISOR", seg_longitud);
                        ii.putExtra("TIEMPO_RECIBIDO_SUPERVISOR", seg_tiempo);
                        context.startActivity(ii);
                        Toasty.info(context, "Ubicando al supervisor...", Toasty.LENGTH_SHORT, true);
                    }



                }catch (Exception e){
                    Toasty.warning(context,"Este supervisor no tiene registros de ubicaciones.", Toast.LENGTH_LONG, true).show();
                }

            }
        }catch (Exception e){
            Toasty.warning(context,"Este supervisor no tiene registros de ubicaciones.", Toast.LENGTH_LONG, true).show();
        }
    }



}
