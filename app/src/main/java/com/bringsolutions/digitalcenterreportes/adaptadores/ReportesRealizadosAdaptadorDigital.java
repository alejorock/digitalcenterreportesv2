package com.bringsolutions.digitalcenterreportes.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.activitysreportes.RealizarReporte;
import com.bringsolutions.digitalcenterreportes.activitysreportes.VisualizarReporte;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.OrdenServicioSupervisor;
import com.bringsolutions.digitalcenterreportes.objetos.Reporte;

import java.util.List;

public class ReportesRealizadosAdaptadorDigital extends RecyclerView.Adapter<ReportesRealizadosAdaptadorDigital.ViewHolder> {

    //clase viewholder para enlazar componesntes con la vista de los mensajes
    public static class ViewHolder extends RecyclerView.ViewHolder{

        private CardView cardView;
        private TextView tvCardLMFolio, tvCardLMDireccion, tvCardLMFecha;

        public ViewHolder(View itemView) {
            super(itemView);
            //enlanzando elementos
            cardView = itemView.findViewById(R.id.tarjetaReporteDigital);
            tvCardLMFolio = itemView.findViewById(R.id.tvCardLMFolio);
            tvCardLMDireccion = itemView.findViewById(R.id.tvCardLMDireccion);

        }

    }

    public List<Reporte> reporteList;
    public Context context;


    public ReportesRealizadosAdaptadorDigital(List<Reporte> reporteList, Context context) {
        this.reporteList = reporteList;
        this.context = context;
    }

    @NonNull
    @Override
    public ReportesRealizadosAdaptadorDigital.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_reporte,viewGroup,false);

        return new ReportesRealizadosAdaptadorDigital.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReportesRealizadosAdaptadorDigital.ViewHolder viewHolder, final  int i) {

        viewHolder.tvCardLMFolio.setText("Folio: "+reporteList.get(i).getFolio());
        viewHolder.tvCardLMDireccion.setText(reporteList.get(i).getDireccion());

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String claveOrden = reporteList.get(i).getOrden();

                //Activity para visualizar el reporte hecho
                Intent ii = new Intent(context, VisualizarReporte.class);
                ii.putExtra("ID_CLAVE_ORDEN", claveOrden);
                context.startActivity(ii);

            }
        });



    }

    @Override
    public int getItemCount() {
        return reporteList.size();
    }




}
