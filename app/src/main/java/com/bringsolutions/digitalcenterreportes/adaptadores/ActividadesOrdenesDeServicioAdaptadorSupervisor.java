package com.bringsolutions.digitalcenterreportes.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.objetos.ActividadOrdenServicio;

import java.util.List;

public class ActividadesOrdenesDeServicioAdaptadorSupervisor extends RecyclerView.Adapter<ActividadesOrdenesDeServicioAdaptadorSupervisor.ViewHolder>  {

    //clase viewholder para enlazar componesntes con la vista de los mensajes
    public static class ViewHolder extends RecyclerView.ViewHolder{

        private CheckBox checkActividad;
        private CardView tarjetaActividad;

        public ViewHolder(View itemView) {
            super(itemView);
            //enlanzando elementos
            checkActividad = itemView.findViewById(R.id.checkBoxNombreActividad);
            tarjetaActividad = itemView.findViewById(R.id.tarjeta_actividad_supervisor);

        }

    }

    public List<ActividadOrdenServicio> actividadesOrdenServicios;
    public Context context;


    public ActividadesOrdenesDeServicioAdaptadorSupervisor(List<ActividadOrdenServicio> actividadesOrdenServicios, Context context) {
        this.actividadesOrdenServicios = actividadesOrdenServicios;
        this.context = context;
    }

    @NonNull
    @Override
    public ActividadesOrdenesDeServicioAdaptadorSupervisor.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_actividad_orden_servicio_supervisor,viewGroup,false);

        return new ActividadesOrdenesDeServicioAdaptadorSupervisor.ViewHolder(view);
    }

    @Override

    public void onBindViewHolder(@NonNull final ActividadesOrdenesDeServicioAdaptadorSupervisor.ViewHolder viewHolder, final  int i) {

        viewHolder.checkActividad.setText(actividadesOrdenServicios.get(i).getAct_nombre());



    }

    @Override
    public int getItemCount() {
        return actividadesOrdenServicios.size();
    }



}
