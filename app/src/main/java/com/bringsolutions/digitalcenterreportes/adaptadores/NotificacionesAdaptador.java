package com.bringsolutions.digitalcenterreportes.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.objetos.Notificacion;
import java.util.List;

public class NotificacionesAdaptador extends RecyclerView.Adapter<NotificacionesAdaptador.ViewHolder> {


    //clase viewholder para enlazar componesntes con la vista de los mensajes
    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView tvNombreNotificacion, tvDescripcionNotificacion, tvFechaNotificacion, tvPropietarioNotificacion;
        private CardView tarjetaNotificacion;

        public ViewHolder(View itemView) {
            super(itemView);
            //enlanzando elementos
            tvNombreNotificacion = itemView.findViewById(R.id.tvNombreDeNotificacion);
            tvDescripcionNotificacion = itemView.findViewById(R.id.tvDescripcionNotificacion);
            tvFechaNotificacion = itemView.findViewById(R.id.tvFechaNotificacion);
            tarjetaNotificacion = itemView.findViewById(R.id.tarjetaNotificacion);
            tvPropietarioNotificacion =itemView.findViewById(R.id.tvPropietarioNoti);
        }

    }

    public List<Notificacion> notificacionList;
    public Context context;

    public NotificacionesAdaptador(List<Notificacion> notificacionList, Context context) {
        this.notificacionList = notificacionList;
        this.context = context;
    }

    @NonNull
    @Override
    public NotificacionesAdaptador.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_notificacion,viewGroup,false);

        return new NotificacionesAdaptador.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final NotificacionesAdaptador.ViewHolder viewHolder, final  int i) {

        viewHolder.tvNombreNotificacion.setText(notificacionList.get(i).getTitulo_mensaje());
        viewHolder.tvDescripcionNotificacion.setText(notificacionList.get(i).getCuerpo_mensaje());
        viewHolder.tvFechaNotificacion.setText(notificacionList.get(i).getFecha());
        viewHolder.tvPropietarioNotificacion.setText(notificacionList.get(i).getPropietarioNoti());



    }

    @Override
    public int getItemCount() {
        return notificacionList.size();
    }



}
