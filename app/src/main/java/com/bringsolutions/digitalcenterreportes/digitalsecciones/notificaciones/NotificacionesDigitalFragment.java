package com.bringsolutions.digitalcenterreportes.digitalsecciones.notificaciones;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.adaptadores.NotificacionesAdaptador;
import com.bringsolutions.digitalcenterreportes.adaptadores.ReportesRealizadosAdaptadorDigital;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.Notificacion;
import com.bringsolutions.digitalcenterreportes.objetos.Reporte;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class NotificacionesDigitalFragment extends Fragment {
    View view;

    FloatingActionButton botonVaciarNotis;

    private List<Notificacion> listaNotificaciones;
    private NotificacionesAdaptador notificacionesAdaptador;

    LinearLayout lnContenido, lnLoading, sinNotisLinear;
    RelativeLayout layoutBotonVaciar;
    private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notificaciones_digital, container, false);


        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayoutDigital);
        botonVaciarNotis = view.findViewById(R.id.botonFlotanteVaciarNotificacionesDigital);
        sinNotisLinear = view.findViewById(R.id.sinNotisLottieDigital);
        layoutBotonVaciar =view.findViewById(R.id.relativeLayoutBotonVaciar);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Esto se ejecuta cada vez que se realiza el gesto
                listaNotificaciones.clear();
                obtenerDatos();
                swipeRefreshLayout.setRefreshing(false);
            }

        });

        final RecyclerView recyclerView = view.findViewById(R.id.recyclerNotificacionesDigital);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        recyclerView.setHasFixedSize(true);

        //recyclerView.setLayoutManager(linearLayoutManager);
        listaNotificaciones = new ArrayList<>();
        notificacionesAdaptador = new NotificacionesAdaptador(listaNotificaciones, getActivity());
        recyclerView.setAdapter(notificacionesAdaptador);
        //listaReportes.clear();

        lnContenido = view.findViewById(R.id.lnContenidoDigital);
        lnLoading   = view.findViewById(R.id.lnLoadingDigitalNoti);

        botonVaciarNotis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaNotificaciones.clear();
                notificacionesAdaptador.notifyDataSetChanged();
                sinNotisLinear.setVisibility(View.VISIBLE);
                layoutBotonVaciar.setVisibility(View.GONE);

            }
        });

        obtenerDatos();

        return view;
    }


    private void obtenerDatos() {
        lnLoading.setVisibility(View.VISIBLE);
        lnContenido.setVisibility(View.GONE);
        AsyncHttpClient cliente = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"mostrar_informes_x_usuario.php?id_user="+Constantes.USUARIO_INICIO_SESION_LOGIN.getIdUsuario();
        cliente.get(url, null, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                layoutBotonVaciar.setVisibility(View.VISIBLE);

                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                layoutBotonVaciar.setVisibility(View.GONE);

                Log.e("Error conexión", error.getLocalizedMessage());
            }
        });

    }

    private void obtieneJSON(String response) {
        try{
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length();i++){

                JSONObject js = jsonArray.getJSONObject(i);

                String remitente_id     = js.getString("remitente_id");
                String titulo_mensaje     = js.getString("titulo_mensaje");
                String cuerpo_mensaje     = js.getString("cuerpo_mensaje");
                String fecha     = js.getString("fecha");
                String destinatario_id     = js.getString("destinatario_id");
                String propietario_noti     = js.getString("username");

                Notificacion noti = new Notificacion();
                    noti.setTitulo_mensaje(titulo_mensaje);
                    noti.setCuerpo_mensaje(cuerpo_mensaje);
                    noti.setFecha(fecha);
                    noti.setPropietarioNoti(propietario_noti);

                    listaNotificaciones.add(noti);
                    notificacionesAdaptador.notifyDataSetChanged();
                    lnContenido.setVisibility(View.VISIBLE);
                    lnLoading.setVisibility(View.GONE);

            }
        }catch (Exception e){
            Log.e("Error conexion", e.getLocalizedMessage());
        }
    }




}
