package com.bringsolutions.digitalcenterreportes.digitalsecciones.ordenes;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bringsolutions.digitalcenterreportes.R;

public class OrdenesDigitalFragment extends Fragment {

    View view ;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_ordenes_digital, container, false);

        return view;
    }

}
