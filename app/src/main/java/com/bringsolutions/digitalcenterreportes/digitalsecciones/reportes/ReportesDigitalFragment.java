package com.bringsolutions.digitalcenterreportes.digitalsecciones.reportes;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.adaptadores.ReportesRealizadosAdaptadorDigital;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.Reporte;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ReportesDigitalFragment extends Fragment {
    View view;


    private List<Reporte> listaReportes;
    private ReportesRealizadosAdaptadorDigital reportesRealizadosAdaptadorDigital;

    LinearLayout lnContenido, lnLoading;
    //private SwipeRefreshLayout swipeRefreshLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.fragment_reportes_digital, container, false);

        /*swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Esto se ejecuta cada vez que se realiza el gesto
                listaReportes.clear();
                obtenerDatos();
                swipeRefreshLayout.setRefreshing(false);
            }

        });*/

        RecyclerView recyclerView = view.findViewById(R.id.recycler_listado_moctezuma);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        //recyclerView.setLayoutManager(linearLayoutManager);
        listaReportes = new ArrayList<>();
        reportesRealizadosAdaptadorDigital = new ReportesRealizadosAdaptadorDigital(listaReportes, getActivity());
        recyclerView.setAdapter(reportesRealizadosAdaptadorDigital);
        //listaReportes.clear();

        lnContenido = view.findViewById(R.id.lnContenido);
        lnLoading   = view.findViewById(R.id.lnLoading);

        obtenerDatos();


        return view;
    }

    private void obtenerDatos() {
        lnLoading.setVisibility(View.VISIBLE);
        lnContenido.setVisibility(View.GONE);
        AsyncHttpClient cliente = new AsyncHttpClient();
        String url = Constantes.URL_PRINCIPAL +"mostrar_reportes.php";
        cliente.get(url, null, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtieneJSON(new String(responseBody));
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("Error conexión", error.getLocalizedMessage());
            }
        });

    }

    private void obtieneJSON(String response) {
        try{
            JSONArray jsonArray = new JSONArray(response);
            for (int i=0; i<jsonArray.length();i++){
                JSONObject js = jsonArray.getJSONObject(i);

                String rep_fkorden     = js.getString("rep_fkorden");
                String rep_folio     = js.getString("rep_folio");
                String rep_direccion     = js.getString("rep_direccion");
                String rep_latitud     = js.getString("rep_latitud");
                String rep_longitud     = js.getString("rep_longitud");
                String rep_evidencia_inicial_uno     = js.getString("rep_evidencia_inicial_uno");
                String rep_evidencia_inicial_dos     = js.getString("rep_evidencia_inicial_dos");
                String rep_evidencia_final_uno     = js.getString("rep_evidencia_final_uno");
                String rep_evidencia_final_dos     = js.getString("rep_evidencia_final_dos");
                String rep_firmavobo     = js.getString("rep_firmavobo");
                String rep_ine_anverso     = js.getString("rep_ine_anverso");
                String rep_ine_reverso     = js.getString("rep_ine_reverso");

                if(rep_folio.equals("null")==false) {
                    Reporte re = new Reporte();
                    re.setOrden(rep_fkorden);
                    re.setFolio(rep_folio);
                    re.setDireccion(rep_direccion);
                    re.setLatitud(rep_latitud);
                    re.setLongitud(rep_longitud);
                    re.setFoto_evidencia_inicial_uno(rep_evidencia_inicial_uno);
                    re.setFoto_evidencia_inicial_dos(rep_evidencia_inicial_dos);
                    re.setFoto_evidencia_final_uno(rep_evidencia_final_uno);
                    re.setFoto_evidencia_final_dos(rep_evidencia_final_dos);
                    re.setFirmavobo(rep_firmavobo);
                    re.setFoto_ine_anverso(rep_ine_anverso);
                    re.setFoto_ine_reverso(rep_ine_reverso);

                    listaReportes.add(re);

                    reportesRealizadosAdaptadorDigital.notifyDataSetChanged();

                    lnContenido.setVisibility(View.VISIBLE);
                    lnLoading.setVisibility(View.GONE);
                }
            }
        }catch (Exception e){
            Log.e("Error conexion", e.getLocalizedMessage());
        }
    }





}
