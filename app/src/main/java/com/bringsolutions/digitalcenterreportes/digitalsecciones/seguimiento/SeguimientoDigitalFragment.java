package com.bringsolutions.digitalcenterreportes.digitalsecciones.seguimiento;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.adaptadores.SupervisoresAdaptador;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.bringsolutions.digitalcenterreportes.objetos.ObjetoSupervisor;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SeguimientoDigitalFragment extends Fragment {
    View view;

    List<ObjetoSupervisor> supervisoresLista = new ArrayList<>();

    RecyclerView recyclerSupervisores;
    SupervisoresAdaptador supervisoresAdaptador;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_seguimiento_digital, container, false);
        view.findViewById(R.id.tvTextoInstruccionesSeguimientoSupervisores).setVisibility(View.GONE);

        recyclerSupervisores = view.findViewById(R.id.recyclerViewSupervisoresSeguimiento);

        poblarRecyclerSupervisores();



        return view;
    }

    private void poblarRecyclerSupervisores() {
        try {
            //Crear un objeto requetqueue
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            //Url del webservice
            String url = Constantes.URL_PRINCIPAL + "mostrar_supervisores.php";

            // crear un objeto requet
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    JSONArray json = response.optJSONArray("supervisores");
                    JSONObject jsonObject = null;
                    try {
                        for (int i = 0; i < json.length(); i++) {
                            jsonObject = json.getJSONObject(i);

                            String id = jsonObject.optString("id");
                            String use_nombre = jsonObject.optString("use_nombre");
                            String username = jsonObject.optString("username");
                            String STATUS = jsonObject.optString("STATUS");
                            String email = jsonObject.optString("email");
                            String use_fkger = jsonObject.optString("use_fkger");

                            supervisoresLista.add(new ObjetoSupervisor(id, use_nombre, username, STATUS, email, use_fkger));

                            if (getRotation(getContext()).equals("vertical")) {
                                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                                recyclerSupervisores.setLayoutManager(layoutManager);
                            } else {
                                recyclerSupervisores.setLayoutManager(new GridLayoutManager(getContext(), 2));
                            }

                            supervisoresAdaptador = new SupervisoresAdaptador(supervisoresLista, getContext());
                            recyclerSupervisores.setAdapter(supervisoresAdaptador);

                            view.findViewById(R.id.tvTextoInstruccionesSeguimientoSupervisores).setVisibility(View.VISIBLE);

                        }
                    } catch (Exception e) {
                        System.out.println("Error nullos: " + e.toString());
                      //  Toast.makeText(getActivity(), "Sin conexión a internet, intente más tarde ", Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("Error Response volley: " + error.toString());

                    //Toast.makeText(getActivity(), "Sin conexión a internet, intente más tarde", Toast.LENGTH_SHORT).show();
                }
            });

            //añadir el objeto requet al requetqueue
            requestQueue.add(jsonObjectRequest);

        }catch (Exception e){

        }
    }

    public String getRotation(Context context){
        final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        switch (rotation) {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                return "vertical";
            case Surface.ROTATION_90:
            default:
                return "horizontal";
        }
    }



}
