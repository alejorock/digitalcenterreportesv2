package com.bringsolutions.digitalcenterreportes.activitysprincipales;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.bringsolutions.digitalcenterreportes.R;

public class Moctezuma extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moctezuma);
        overridePendingTransition(R.transition.transicion_entrada, R.transition.transicion_salida);

    }
}
