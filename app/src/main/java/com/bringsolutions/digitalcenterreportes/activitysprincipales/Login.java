package com.bringsolutions.digitalcenterreportes.activitysprincipales;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.bd.SQLHelper;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Login extends AppCompatActivity {


    private int tiempo =1200000; //milisegundos = 20 min

    private Button btnInciarSesion, btnOlvidePass, btnModoOffline;
    private EditText cajaUsuario, cajaPassword;
    private LinearLayout lnCargando;

    //Datos Volley
    private RequestQueue requestQueue;
    private StringRequest stringRequest;

    //Datos Geolocalización
    public static LocationManager locationManager;
    private Geocoder geocoder;
    private List<Address> addresses;

    CheckBox mantenerSesion;

    private static final String PREFERENCIAS_SESION = "preferencias_sesion ";
    private static final String ESTADO_BOTON = "estado.boton.mantenersesion ";
    private static String TIPO_USUARIO = "null";
    private static String PREFERENCIAS_ID = "null";

    AlertDialog dialog;
    SQLHelper sqlHelper;
    Constantes constantes = new Constantes();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sqlHelper = new SQLHelper(this,SQLHelper.BASE_DATOS_NOMBRE, null, SQLHelper.BASE_DATOS_VERSION);
        if (constantes.checkInternet(getApplicationContext())){
            if (obtenerEstadoSesion()){
                if (obtenerPreferenciasTipo().contains("0")){
                    Constantes.STATUS_ENVIO_UBICACION=true;
                    activarEnvioCoordenadas(tiempo);
                    Toasty.success(getApplicationContext(), "¡Bienvenido Supervisor!", Toast.LENGTH_SHORT, true).show();
                    startActivity(new Intent(this, Supervisor.class));
                    overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
                    finish();
                }else if (obtenerPreferenciasTipo().contains("1")){
                    Toasty.success(getApplicationContext(), "¡Bienvenido Digital Center!", Toast.LENGTH_SHORT, true).show();
                    startActivity(new Intent(this, Digital.class));
                    overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
                    finish();
                }else if(obtenerPreferenciasTipo().contains("3")){
                    Toasty.success(getApplicationContext(), "¡Bienvenido Moctezuma!", Toast.LENGTH_SHORT, true).show();
                    startActivity(new Intent(this, Digital.class));
                    overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
                    finish();
                }
            }
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        inicializarComponentes();
        clicks();

    }

    private void clicks() {
        btnModoOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Login.this, "En construcción...", Toast.LENGTH_SHORT).show();
            }
        });

        btnOlvidePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogoPassOlvidada();

            }
        });

        btnInciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialogCargando();
                String usuarioProporcionado=cajaUsuario.getText().toString(), passwordProporcionado=cajaPassword.getText().toString();
                if(usuarioProporcionado.isEmpty() || passwordProporcionado.isEmpty()){
                    Toasty.warning(getApplicationContext(),"Algún campo vacío", Toast.LENGTH_SHORT, true).show();
                }else{
                    if (constantes.checkInternet(getApplicationContext())){
                        validarUsuario(cajaUsuario.getText().toString(), cajaPassword.getText().toString());
                        btnInciarSesion.setVisibility(View.GONE);
                        lnCargando.setVisibility(View.VISIBLE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }else{
                        checarDatosOff(cajaUsuario.getText().toString(), cajaPassword.getText().toString());
                        btnInciarSesion.setVisibility(View.GONE);
                        lnCargando.setVisibility(View.VISIBLE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                }

            }
        });
    }

    private void dialogoPassOlvidada() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialogo_recuperar_password, null);

        builder.setView(viewDialog);

        final EditText etIdEmpleado = viewDialog.findViewById(R.id.etIdEmpleadoDialogContra);
        Button btnCancelar = viewDialog.findViewById(R.id.btnCancelarContra);
        Button btnRecuperar = viewDialog.findViewById(R.id.btnRecuperarContra);

        final AlertDialog dialog = builder.create();

        btnRecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String idempleado = etIdEmpleado.getText().toString();
                Toast.makeText(Login.this, "Si el ID existe se enviará un vínculo al correo vinculado al ID", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void inicializarComponentes() {
        mantenerSesion  = findViewById(R.id.mantenerSesion);

        lnCargando      = findViewById(R.id.lnLoading);
        cajaPassword = findViewById(R.id.cajaPassword);
        cajaUsuario = findViewById(R.id.cajaUsuario);
        btnInciarSesion = findViewById(R.id.btnIniciarSesion);
        btnModoOffline = findViewById(R.id.btnModoOffline);
        btnOlvidePass = findViewById(R.id.btnOlvideContra);
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        verificarPermisosGenerales();


    }

    /*private boolean validarUsuario(final String usuario, final String password){
        String url = "http://www.dc.vigux.com.mx/site/swlogin?user="+usuario+"&pass="+password;

        stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                obtenerDatos(response);
                respuestaExistenciaUsuario=true;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    checarDatosOff(usuario, password);

                }catch (Exception e){
                    Log.e("error", e.getLocalizedMessage());
                }

                //Toasty.warning(getApplicationContext(),"Hubo un problema al iniciar sesión, intente más tarde.", Toast.LENGTH_LONG, true).show();
            }
        });

        requestQueue.add(stringRequest);

        //RETORNO EL VALOR FINAL
        return  respuestaExistenciaUsuario;

    }*/
    private void validarUsuario(String usuario, String password){
        AsyncHttpClient client = new AsyncHttpClient();
        String url = "http://www.dc.vigux.com.mx/site/swlogin?user="+usuario+"&pass="+password;

        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                    obtenerDatos(new String(responseBody));

                }else{
                    btnInciarSesion.setVisibility(View.VISIBLE);
                    lnCargando.setVisibility(View.GONE);
                    Toasty.error(getApplicationContext(), "No hay datos sincronizados", Toasty.LENGTH_LONG).show();
                    //dialog.dismiss();

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toasty.warning(getApplicationContext(),"Hubo un problema al iniciar sesión, intente más tarde.", Toast.LENGTH_LONG, true).show();
                //dialog.dismiss();

                btnInciarSesion.setVisibility(View.VISIBLE);
                lnCargando.setVisibility(View.GONE);
            }
        });
    }


    private void obtenerDatos(String response) {

        //prepaparDatos(cajaUsuario.getText().toString(), cajaPassword.getText().toString());

        //TIPOS DE USUARIO
        //0 = Supervisor
        //1 = Digital
        //3 = Moctezuma

        try {
            JSONObject jsonObject = new JSONObject(response);

            String id     = jsonObject.getString("id");
            String nombre = jsonObject.getString("nombre");
            String tipo   = jsonObject.getString("tipo");

            Constantes.USUARIO_INICIO_SESION_LOGIN.setIdUsuario(id);
            Constantes.USUARIO_INICIO_SESION_LOGIN.setTipoUsuario(tipo);
            Constantes.USUARIO_INICIO_SESION_LOGIN.setNombreUsuario(nombre);

            guardarPreferenciasMantenerSesion();

            registrarUsuariosOffline(id, nombre, tipo);
            if (tipo.equals("0")){
                Constantes.STATUS_ENVIO_UBICACION=true;

                //ACTIVAR UBICACION CADA CIERTO TIEMPO
                activarEnvioCoordenadas(tiempo);
                Toasty.success(getApplicationContext(), "¡Bienvenido Supervisor!", Toast.LENGTH_SHORT, true).show();
                startActivity(new Intent(this, Supervisor.class));
                overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
                guardarPreferenciasdeUsuario(tipo);
                guardarPreferenciasID(id);

                finish();
            }else if(tipo.equals("1")){
                Toasty.success(getApplicationContext(), "¡Bienvenido Digital Center!", Toast.LENGTH_SHORT, true).show();
                startActivity(new Intent(this, Digital.class));
                overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
                guardarPreferenciasdeUsuario(tipo);
                guardarPreferenciasID(id);

                finish();

            }else if(tipo.equals("3")){
                Toasty.success(getApplicationContext(), "¡Bienvenido Moctezuma!", Toast.LENGTH_SHORT, true).show();
                startActivity(new Intent(this, Digital.class));
                overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
                guardarPreferenciasdeUsuario(tipo);
                guardarPreferenciasID(id);

                finish();
            }

        } catch (JSONException ex) {
            Toasty.warning(getApplicationContext(),"No hay datos sincronizados...", Toast.LENGTH_LONG, true).show();
            btnInciarSesion.setVisibility(View.VISIBLE);
            lnCargando.setVisibility(View.GONE);
        }
    }

    private void activarEnvioCoordenadas(int tiempo) {
        if(Constantes.STATUS_ENVIO_UBICACION || Constantes.USUARIO_INICIO_SESION_LOGIN.getTipoUsuario().equals("0")) {
            try {
                final Handler handler = new Handler();
                Timer timer = new Timer();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    activarGeolocalizacion();
                                    Thread.sleep(5000);
                                    mandarUbicacionBD();
                                    locationManager.removeUpdates(locationListenerGPS);
                                   // dialog.dismiss();
                                } catch (Exception e) {}
                            }
                        });
                    }
                };
                timer.schedule(task, 0, tiempo);
            } catch (Exception e) {}
        }

    }

    private void mandarUbicacionBD() {
        String url="";
        AsyncHttpClient client = new AsyncHttpClient();

        if(Constantes.USUARIO_INICIO_SESION_LOGIN.getLatitudActual()==null || Constantes.USUARIO_INICIO_SESION_LOGIN.getLongitudActual()==null){
            url=Constantes.URL_PRINCIPAL+"actualizar_ubicacion.php?latitud=17.994721666666667&longitud=-92.96210500000001&fksupervisor="+Constantes.USUARIO_INICIO_SESION_LOGIN.getIdUsuario();
        }else{
            url=Constantes.URL_PRINCIPAL+"actualizar_ubicacion.php?latitud="+Constantes.USUARIO_INICIO_SESION_LOGIN.getLatitudActual()+"&longitud="+Constantes.USUARIO_INICIO_SESION_LOGIN.getLongitudActual()+"&fksupervisor="+Constantes.USUARIO_INICIO_SESION_LOGIN.getIdUsuario();
        }
        url= url.replace(" ", "%20");


        client.get(url, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200){
                   // Toasty.custom(Login.this,"Ubicación enviada!",R.drawable.ic_marcador_mapa_icono,R.color.colorPrimaryDark,4000,true,true).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
        /*stringRequest= new StringRequest(StringRequest.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toasty.custom(Login.this,"Ubicación enviada!",R.drawable.ic_marcador_mapa_icono,R.color.colorPrimaryDark,4000,true,true).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        requestQueue.add(stringRequest);*/
    }

    //GPS

    public void activarGeolocalizacion( ) {
        if (!ubicacionActiva())
            return;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }
        Constantes.STATUS_ENVIO_UBICACION=true;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 20 * 1000, 10, locationListenerGPS);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,2 * 20 * 1000, 10, locationListenerGPS);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,2 * 20 * 1000, 10, locationListenerGPS);

    }

    public  final  LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            Constantes.USUARIO_INICIO_SESION_LOGIN.setLatitudActual(location.getLatitude());
            Constantes.USUARIO_INICIO_SESION_LOGIN.setLongitudActual(location.getLongitude());
            Constantes.OBJETO_REPORTE.setLatitud(String.valueOf(location.getLatitude()));
            Constantes.OBJETO_REPORTE.setLongitud(String.valueOf(location.getLongitude()));

            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 100);
                String direccion = addresses.get(0).getAddressLine(0);
                String codigopostal= addresses.get(0).getPostalCode();
                if(codigopostal==null || codigopostal.equals("null") || codigopostal.isEmpty()|| codigopostal==""){
                    codigopostal="SIN CP";
                }
                String   direccionActual = direccion+", "+codigopostal;
                Constantes.OBJETO_REPORTE.setDireccion(direccionActual);

            }catch (Exception e){
                Toasty.info(getApplicationContext(), "Ha ocurrido un problema a la hora de obtener la dirección actual, por favor intente nuevamente...", Toast.LENGTH_SHORT, true).show();

            }
            obtenerDatosGPS(Constantes.USUARIO_INICIO_SESION_LOGIN.getLatitudActual(),Constantes.USUARIO_INICIO_SESION_LOGIN.getLongitudActual());



        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }
        @Override
        public void onProviderDisabled(String s) {
            Toasty.info(getApplicationContext(), "Es necesaria su ubicación, actívela", Toast.LENGTH_LONG, true).show();
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);        }
    };

    private void obtenerDatosGPS(Double latitud, Double longitud) {
        geocoder = new Geocoder(this, Locale.getDefault());
        try
        {
            addresses = geocoder.getFromLocation(latitud, longitud, 100);

            String direccion    = addresses.get(0).getAddressLine(0);
            String codigopostal = addresses.get(0).getPostalCode();
            String ubicacion    = direccion+", "+codigopostal;

            Constantes.USUARIO_INICIO_SESION_LOGIN.setDireccionActual(ubicacion);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   private boolean proveerGPS() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)|| locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
    }

    private boolean ubicacionActiva() {

        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            Toasty.info(this, "Es necesaria su ubicación, actívela", Toast.LENGTH_LONG, true).show();
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
        }
        return proveerGPS();



    }

    private void verificarPermisosGenerales(){

        int permisosRequestCode = 100;

        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        int accessFinePermission = ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCoarsePermission = ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION);
        int externalStoragePermission = ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA);

        if (externalStoragePermission == PackageManager.PERMISSION_GRANTED && accessFinePermission == PackageManager.PERMISSION_GRANTED && accessCoarsePermission == PackageManager.PERMISSION_GRANTED && cameraPermission == PackageManager.PERMISSION_GRANTED) {
            //se realiza metodo si es necesario...
        } else {
            ActivityCompat.requestPermissions(this,perms, permisosRequestCode);
        }




    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {

        switch (requestCode) {
            case 100:

                break;

        }
    }

    //Login para momentos offline
    private void registrarUsuariosOffline(String id, String usuario, String tipo){
        SQLiteDatabase db = sqlHelper.getWritableDatabase();
        String insert = "INSERT INTO "+Constantes.TABLA_USUARIOS_LOCAL+" " +
                "("+Constantes.CAMPO_USUARIO_ID+", "+Constantes.CAMPO_USUARIO_NOMBRE+", "+Constantes.CAMPO_USUARIO_TIPO+") "+
                "VALUES ("+id+", '"+usuario+"', '"+tipo+"')";


        db.execSQL(insert);
        db.close();
    }

    private void checarDatosOff(String usuario, String pass){
            SQLiteDatabase db = sqlHelper.getReadableDatabase();
            String[] parametros = {usuario};

            Cursor cursor = db.rawQuery("SELECT * FROM "+Constantes.TABLA_USUARIOS_LOCAL+" " +
                    "WHERE "+Constantes.CAMPO_USUARIO_NOMBRE+" = ?",parametros);
            guardarPreferenciasMantenerSesion();

            cursor.moveToLast();
            try {
                if (cursor.getString(2).equals("0")){
                    Constantes.STATUS_ENVIO_UBICACION=true;

                    //ACTIVAR UBICACION CADA CIERTO TIEMPO
                    //activarEnvioCoordenadas(tiempo);
                    Toasty.success(getApplicationContext(), "¡Bienvenido Supervisor!", Toast.LENGTH_SHORT, true).show();
                    startActivity(new Intent(this, Supervisor.class));
                   // guardarPreferenciasdeUsuario(cursor.getString(2));

                    //dialog.dismiss();

                    finish();
                }else if(cursor.getString(2).equals("1")){
                    Toasty.success(getApplicationContext(), "¡Bienvenido Digital Center!", Toast.LENGTH_SHORT, true).show();
                    startActivity(new Intent(this, Digital.class));
                    //guardarPreferenciasdeUsuario(cursor.getString(3));
                    //dialog.dismiss();
                    finish();

                }else if(cursor.getString(2).equals("3")){

                }
            }catch (Exception e){
                Toasty.error(getApplicationContext(), "No hay datos sincronizados \nConectate a Internet para poder sincronizar la app", Toasty.LENGTH_LONG).show();
                //dialog.dismiss();
                btnInciarSesion.setVisibility(View.VISIBLE);
                lnCargando.setVisibility(View.GONE);
            }
    }

    private void dialogCargando() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = getLayoutInflater();
        final View viewDialog = layoutInflater.inflate(R.layout.dialog_cargando, null);
        builder.setView(viewDialog);
        dialog = builder.create();
        dialog.show();

    }

    private void guardarPreferenciasMantenerSesion(){
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCIAS_SESION, MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(ESTADO_BOTON, mantenerSesion.isChecked()).apply();
    }
    private boolean obtenerEstadoSesion(){
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCIAS_SESION, MODE_PRIVATE);
        return sharedPreferences.getBoolean(ESTADO_BOTON, false);

    }
    public static void cambiarPreferenciaSesion(Context context, boolean b){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCIAS_SESION, MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(ESTADO_BOTON, b).apply();
    }

    public void guardarPreferenciasdeUsuario(String tipo){
        SharedPreferences sharedPreferences = getSharedPreferences(TIPO_USUARIO, MODE_PRIVATE);
        sharedPreferences.edit().putString(TIPO_USUARIO, tipo).apply();
    }
    public String obtenerPreferenciasTipo(){
        SharedPreferences sharedPreferences = getSharedPreferences(TIPO_USUARIO, MODE_PRIVATE);
        return sharedPreferences.getString(TIPO_USUARIO, "null");
    }

    public void guardarPreferenciasID(String id){
        SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCIAS_ID, MODE_PRIVATE);
        sharedPreferences.edit().putString(PREFERENCIAS_ID, id).apply();
    }
    public static String obtenenerPreferenciaID(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCIAS_ID, MODE_PRIVATE);
        return sharedPreferences.getString(PREFERENCIAS_ID, "null");
    }

}
