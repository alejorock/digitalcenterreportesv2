package com.bringsolutions.digitalcenterreportes.activitysprincipales;

import android.content.Intent;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
public class Supervisor extends AppCompatActivity {
    private TextView tvDireccionActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervisor);
        overridePendingTransition(R.transition.transicion_entrada, R.transition.transicion_salida);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        inicializarElementos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_toolbar:
                Intent i  = new Intent(Supervisor.this, Login.class);
                Constantes.OBJETO_REPORTE.setUsuario(null);
                Constantes.OBJETO_REPORTE.setDireccion(null);
                Constantes.OBJETO_REPORTE.setLatitud(null);
                Constantes.OBJETO_REPORTE.setLongitud(null);
                Constantes.OBJETO_REPORTE.limpiarDatosReporte();
                Constantes.STATUS_ENVIO_UBICACION=false;
                overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);
                i.putExtra("valor_status_ubicacion","0");
                Login.cambiarPreferenciaSesion(Supervisor.this, false);

                startActivity(i);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void inicializarElementos() {
        tvDireccionActual = findViewById(R.id.tvDireccionUbicacionActualSupervisor);
        tvDireccionActual.setText(Constantes.USUARIO_INICIO_SESION_LOGIN.getDireccionActual());
        BottomNavigationView navView = findViewById(R.id.nav_view_supervisor);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder( R.id.navigation_ordenes_supervisor, R.id.navigation_localizaciones_supervisor, R.id.navigation_notificaciones_supervisor).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_supervisor);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
