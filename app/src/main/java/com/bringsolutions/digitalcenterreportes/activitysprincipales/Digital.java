package com.bringsolutions.digitalcenterreportes.activitysprincipales;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.bringsolutions.digitalcenterreportes.R;
import com.bringsolutions.digitalcenterreportes.objetos.Constantes;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class Digital extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_toolbar:
                Intent i  = new Intent(Digital.this, Login.class);
                Constantes.OBJETO_REPORTE.setUsuario(null);
                Constantes.OBJETO_REPORTE.setDireccion(null);
                Constantes.OBJETO_REPORTE.setLatitud(null);
                Constantes.OBJETO_REPORTE.setLongitud(null);
                Constantes.OBJETO_REPORTE.limpiarDatosReporte();
                Login.cambiarPreferenciaSesion(Digital.this, false);

                overridePendingTransition(R.transition.transicion_salida, R.transition.transicion_entrada);

                startActivity(i);
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital);
//        overridePendingTransition(R.transition.transicion_entrada, R.transition.transicion_salida);


        BottomNavigationView navView = findViewById(R.id.nav_view_digital);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder( R.id.navigation_ordenes_digital, R.id.navigation_reportes_digital, R.id.navigation_seguimiento_digital, R.id.navigation_notificaciones_digital).build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        NavigationUI.setupWithNavController(navView, navController);

    }

}
