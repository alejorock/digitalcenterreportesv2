package com.bringsolutions.digitalcenterreportes.bd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.bringsolutions.digitalcenterreportes.objetos.Constantes;

public class SQLHelper extends SQLiteOpenHelper {

    String tablaRecordarPass = "CREATE TABLE recordarpass(idrecordar INTEGER PRIMARY KEY AUTOINCREMENT, estado TEXT, usuario TEXT, pass TEXT)";
    String tablaReporteCreado = "CREATE TABLE reporte_creado(idreporte INTEGER PRIMARY KEY AUTOINCREMENT, clave TEXT, usuario TEXT, folio TEXT, " +
                                            "direccion TEXT, latitud TEXT, longitud TEXT, foto_evidencia_inicial_uno TEXT, " +
                                            "foto_evidencia_inicial_dos TEXT, foto_evidencia_final_uno TEXT, foto_evidencia_final_dos TEXT, " +
                                            "fimavobo TEXT, foto_ine_anverso TEXT, foto_ine_reverso TEXT, orden TEXT )";

    public static final String BASE_DATOS_NOMBRE = "bd_digital";

    public static final int BASE_DATOS_VERSION = 1;

    public SQLHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tablaRecordarPass);
        db.execSQL(Constantes.CREAR_TABLA_REPORTE_CREADO);
        db.execSQL(Constantes.CREAR_TABLA_ORDENES_LOCAL);
        db.execSQL(Constantes.CREAR_TABLA_ORDENES_DIGITAL_LOCAL);
        db.execSQL(Constantes.CREAR_TABLA_ACTIVIDADES_ORDEN);
        db.execSQL(Constantes.CREAR_TABLA_USUARIOS_LOCAL);

        db.execSQL("INSERT INTO recordarpass (estado ,usuario, pass) values ('false','sinusuario','sinpass')");

        //db.execSQL("INSERT INTO reporte_creado(clave, usuario, folio, direccion, latitud, longitud, foto_evidencia_inicial_uno, foto_evidencia_inicial_dos," +
        //        "foto_evidencia_final_uno, foto_evidencia_final_dos, firmavobo, foto_ine_anverso, foto_ine_reverso, orden) values ");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS recordarpass");
        db.execSQL("DROP TABLE IF EXISTS "+Constantes.TABLA_REPORTE_CREADO);
        db.execSQL("DROP TABLE IF EXISTS "+Constantes.TABLA_ORDENES_SUPERVISOR_LOCAL);
        db.execSQL("DROP TABLE IF EXISTS "+Constantes.TABLA_ORDENES_DIGITAL_LOCAL);
        db.execSQL("DROP TABLE IF EXISTS "+Constantes.TABLA_ACTIVIDAD_ORDEN_LOCAL);
        db.execSQL("DROP TABLE IF EXISTS "+Constantes.TABLA_USUARIOS_LOCAL);

        onCreate(db);
    }
}
