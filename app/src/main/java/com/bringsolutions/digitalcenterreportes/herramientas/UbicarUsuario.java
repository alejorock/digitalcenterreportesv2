package com.bringsolutions.digitalcenterreportes.herramientas;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import com.bringsolutions.digitalcenterreportes.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class UbicarUsuario extends AppCompatActivity {
    SupportMapFragment mapFragment;

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicar_usuario);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intentRecibir= getIntent();

        String LATITUD_RECIBIDA_SUPERVISOR = intentRecibir.getStringExtra("LATITUD_RECIBIDA_SUPERVISOR");
        String LONGITUD_RECIBIDA_SUPERVISOR = intentRecibir.getStringExtra("LONGITUD_RECIBIDA_SUPERVISOR");
        final String TIEMPO_RECIBIDO_SUPERVISOR = intentRecibir.getStringExtra("TIEMPO_RECIBIDO_SUPERVISOR");

        final LatLng latLng = new LatLng(Double.parseDouble(LATITUD_RECIBIDA_SUPERVISOR), Double.parseDouble(LONGITUD_RECIBIDA_SUPERVISOR));

        mapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapillaUsuario);

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(latLng).title("Última Ubicación registrada").snippet("Fecha y hora: " +TIEMPO_RECIBIDO_SUPERVISOR));
                CameraUpdate animacionZoom = CameraUpdateFactory.newLatLngZoom(latLng,18);
                googleMap.animateCamera(animacionZoom);
            }
        });


    }





}
