package com.bringsolutions.digitalcenterreportes.herramientas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.bringsolutions.digitalcenterreportes.R;
import com.bumptech.glide.Glide;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ZoomImageView extends AppCompatActivity {
    ImageView imgZoomView;
    PhotoViewAttacher photoViewAttacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imgZoomView = findViewById(R.id.vistaImagenZoom);
        Intent intentRecibir= getIntent();
        String foto_inicial_uno_recibida= intentRecibir.getStringExtra("foto_inicial_uno_re");
        String foto_inicial_dos_recibida= intentRecibir.getStringExtra("foto_inicial_dos_re");
        String foto_final_uno_recibida= intentRecibir.getStringExtra("foto_final_uno_re");
        String foto_final_dos_recibida= intentRecibir.getStringExtra("foto_final_dos_re");
        String foto_firma_re = intentRecibir.getStringExtra("foto_firma_re");
        String ine_frente_re = intentRecibir.getStringExtra("ine_frente_re");
        String ine_atras_re =  intentRecibir.getStringExtra("ine_atras_re");


        if (foto_inicial_uno_recibida!=null){
            Glide.with(getApplicationContext()).load(foto_inicial_uno_recibida).into(imgZoomView);
        }

        if (foto_inicial_dos_recibida!=null){
            Glide.with(getApplicationContext()).load(foto_inicial_dos_recibida).into(imgZoomView);
        }

        if (foto_final_uno_recibida!=null){
            Glide.with(getApplicationContext()).load(foto_final_uno_recibida).into(imgZoomView);
        }

        if (foto_final_dos_recibida!=null){
            Glide.with(getApplicationContext()).load(foto_final_dos_recibida).into(imgZoomView);
        }

        if (foto_firma_re!=null){
            Glide.with(getApplicationContext()).load(foto_firma_re).into(imgZoomView);
        }

        if (ine_frente_re!=null){
            Glide.with(getApplicationContext()).load(ine_frente_re).into(imgZoomView);
        }

        if (ine_atras_re!=null){
            Glide.with(getApplicationContext()).load(ine_atras_re).into(imgZoomView);
        }

        photoViewAttacher = new PhotoViewAttacher(imgZoomView);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }
}
